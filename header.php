<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta name="google-site-verification" content="CrccUvevJDQrqCa9xzruEDL20CrZK5EhR2nObNC3UF8" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>


<script>
 	var TEMPLATE_URL = "<?php echo get_template_directory_uri(); ?>";
</script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/js/vendor/glDatePicker-2.0/css/glDatePicker.flatwhite.css'; ?>" type="text/css" media="all">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" type="text/css" media="all">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" type="text/css" media="all">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/ajax-loader.gif" type="text/css" media="all">
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300' rel='stylesheet' type='text/css'>
  
  <script id="script-infochat" data-config="https://control.asksuite.com/api/companies/hotel-arpoador" src="https://cdn.asksuite.com/infochat.js"></script>

  <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '453882681976349');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=453882681976349&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MW5SP6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MW5SP6');</script>
<!-- End Google Tag Manager -->

<div id="drawer">
  <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu-drawer' ) ); ?>   
</div>

	
<div id="mobile-wrap">

    <div id="mobile-icon"></div>
    <!--<div id="mobile-wrap-container">-->

    <div id="mediaquery"></div>
    
	<header id="masthead">
    	<div id="header-container" class="margin-wide">
        	<div class="ext-wrap">				
            	<!-- main menu -->
                <nav id="site-navigation" class="main-navigation" role="navigation">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
                </nav>    			
                <!-- language selector -->
                <?php layout_langswitch(); ?>
            	<!-- logo -->
                <a href="<?php bloginfo( 'url' ); ?>" class="home-link"><div id="logo-small"></div></a>
                <!-- book btn -->
                <div id="book-now-btn" class="btn" data-url="<?php the_field('url-res','options'); ?>" data-hotel="<?php the_field('hotel-res','options'); ?>" data-chain="<?php the_field('chain-res','options'); ?>" data-locale="<?php the_field('locale','options'); ?>"><?php the_field('texto_botao','options'); ?></div>
                
                <div id="line-bottom" class="invisible"></div>
            </div><!-- .ext-wrap -->
        </div><!-- #header-container -->
	</header><!-- #masthead -->