<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
     <div class="clearfix"></div>

	</div><!-- #main .wrapper -->
    
   <?php global $language; ?>
   
	<footer class="ext-wrap section">
        <hr/>
        		
        <div class="row clearfix">        
        	<div class="col3">
            	<div class="book-now-mobile btn"><?php echo $language == 'en' ? 'Book Now →' : 'Reservar Agora →'; ?></div>
            	<ul id="footer-menu">
                	<li>
                    	<a href="<?php echo get_bloginfo( 'url' );?>"><?php echo get_bloginfo( 'name' );?></a>
                    </li>
                	<li>
                    	<a href="<?php $field = $language == 'en' ? 'book_en' : 'book_pt'; $link = get_field( $field , 'options'); echo $link; ?>" ><?php echo $language == 'en' ? 'Book Now' : 'Reservar Agora'; ?></a>
                    </li>
					<?php wp_list_pages('exclude=6,4,77,82,86,773,771&depth=1&title_li='); ?>
                </ul>
                <p class="link-other-hotel">
                    <a href="<? echo get_field('link_hotel', 'options'); if ($language == 'en') : echo '/en'; endif; ?>"><? echo get_field( $language == 'en' ? 'visit_text' : 'visit_text_pt' , 'options' ); ?> <span class="arrow"></span>  <span><? echo get_field('hotel_name', 'options'); ?></span></a>
                </p>
            </div>
        	<div class="col3 directions">
                <?php
                $page = get_page_by_title( $language == 'en' ? 'Contact' : 'Contato' );
				echo apply_filters('the_content', $page->post_content);
				?>
            </div>
        	<div class="col3">
            	<h6 class="margin5"><?php echo $language == 'en' ? 'Follow Us' : 'Siga-nos'; ?></h6>
                <ul id="follow" class="clearfix">
				<?php 
                $socials = get_field('social_links', 'options');
                foreach ( $socials as $social ):
					?>
                    <li><a href="<?php echo $social['link']; ?>"><img src="<?php echo $social['icon']['url']; ?>" /></a></li>
                    <?php
				endforeach;
				?>
                </ul>

            	<h6 class="margin5"><?php echo $language == 'en' ? 'Subscribe To Our Newsletter' : 'Assine a Nossa Newsletter'; ?></h6>
                <form>
                	<ul id="newsletter">
                    	<li><input name="email" type="text"/></li>
                    	<li><input name="Submit" type="submit" value="<?php echo $language == 'en' ? 'Subscribe →' : 'Assine →'; ?>" class="btn"></li>
                    </ul>
                </form>
                
            	<h6><?php echo $language == 'en' ? 'Employment' : 'Emprego'; ?></h6>
                <p><a href="mailto:rh@grupoarpoador.com" class="red">rh@grupoarpoador.com</a></p>
                
            </div>

            <div class="clearfix"></div>

        </div>

        <div class="row clearfix little">        
        	<div class="col3">
        		<?php echo $language == 'en' ? '© ARPOADOR OF HOTELS AND TURISM LTDA' : '© ARPOADOR DE HOTEIS E TURISMO LTDA'; ?>
            </div>
        	<div class="col3">
            	<span class="dst-italic little">Site by</span>&nbsp;&nbsp;<a href="http://zagollc.com">ZAGO</a>
            </div>
        	<div class="col3">
            </div>

            <div class="clearfix"></div>
        </div>
        
        
        <div class="spacer60"></div>

        <div class="clearfix"></div>

	</footer>

    
    <!--</div>  #mobile-wrap-container -->
</div>  <!-- #mobile-wrap -->
    

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZjBxFScnY08Kd9TWJx1C57JSVkq-06Kk&sensor=false"></script>
    <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery-1.10.1.min.js'; ?>"><\/script>')</script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.easing.1.3.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/glDatePicker-2.0/glDatePicker.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/slidesjs/jquery.slides.min.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.imagesloaded.min.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.transit-master/jquery.transit.min.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/wookmark/jquery.wookmark.min.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/twitter/twitterFetcher_v10_min.js'; ?>"></script>    
     <script src="<?php echo get_template_directory_uri() . '/js/main.js?v1.3'; ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.instagram/jquery.instagram.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/main_insta.js'; ?>"></script>
	<script>
    	$(document).ready(function(){
 
  			$('a.link-tripadvisor').click(function(){
    			window.open(this.href);
    		return false;
  			});
		});
    </script>

    <script>
	/* CONTACT PAGE **************************************/
	function initContact(){
		$('#container-map').imagesLoaded( function(){
			$('.marker').each(function(index, element) {
			   $(this).css( {
					left:$(this).position().left - $(this).width()/2,
					top:$(this).position().top - $(this).height()		
				}).transition({opacity:1}, 337, 'easeOutCircle'); 
			});
			$('.indication').hover( function(e){
					$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
				}, 
				function(e){
					$('.map-marker:eq(' + $(this).attr('rel') + ')').removeClass( 'selected' ); 			
				} );
	
			$('.map-marker').hover( function(e){
					setBocadillo( $(this).attr('rel'), true )
				}, 
				function(e){
					setBocadillo( $(this).attr('rel'), false )
				} );
				
			$('.map-marker-big').hover( function(e){
					setBocadillo( -1, true )
				}, 
				function(e){
					setBocadillo( -1, false )
				} );
				
			var $win = $( '#win-marker' );
				
			// ipad
			$('.map-marker').click( function(){
				setBocadillo( $(this).attr('rel'), true );
			} );
			$('.map-marker-big').click( function(){
				setBocadillo( -1, true );
			} );	
			$win.click( function(){ $win.hide(); });	
			
			$('.row .indication').click( function(){
				$('.marker').removeClass( 'selected' );
				$('.indication').removeClass( 'selected' );
				$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
				$(this).addClass( 'selected' );
			})		
			
			function setBocadillo( j, b ){
				var i = parseInt( j );
				var $marker = i < 0 ? $('.map-marker-big') : $('.map-marker:eq(' + i + ')');
				var $map = $('#map');
				
				if ( b ){
					var html = '<ul><li>';
					if ( $marker.attr('data-img') ){
						if ( $marker.attr('data-img') != '' ){
							html += '<img class="map-window" src="'+ $marker.attr('data-img') +'"></li><li>';
						}
					}
					html += (i < 0 ? '<div class="indication" rel="-1"><div class="i-title"><?php echo $language == 'en' ? 'HOTEL ARPOADOR<br>In the heart of Rio' : 'HOTEL ARPOADOR<br>No coração do Rio'; ?></div><div class="i-text"><p><strong></strong></p></div></div>' : $('.row .indication:eq(' + i + ')').clone()[0].outerHTML );
					html += '</li></ul>';
					
					
					$win.empty().html( html );
					
					var left = $marker.position().left - 40;
					if ( left + $win.width() > $map.width() ){
						left = $map.width() - $win.width() - 40;
					}
					var top = $marker.position().top - $win.height() - 40;
					if ( top < 20 ){
						top = 20;
						if ( $marker.position().left > $map.width()/2 ){
							// go left the marker
							left = $marker.position().left - $win.width() - 40;
						}
						else{
							// go right the marker
							left = $marker.position().left + 40;
						}
					}
					$win.css({left: left
						,top: top
					});
					$win.show();
				}
				else{
					$win.hide().empty();
				}
			}
			resizeMap();
		});
	}
	
	function resizeMap(){
		if ( $('#map').length > 0 ){
			var w = $window.width();
			var p;
			if ( w <= 480 ){
				//console.log( 'mobile' )
			}
			else if ( w <= 1024 ){
				//console.log( 'ipad' )
				p = 748/996;
			}
			else{
				//console.log( 'desktop' )
				p = 1;
			}
			if ( p ){
				$('.marker').each(function(index, element) {
					var coor = $(this).attr('data-rel').split( ',' );
					$(this).css( {
						left: parseInt(coor[0])*p,
						top: parseInt(coor[1])*p		
					});
				   $(this).css( {
						left:$(this).position().left - $(this).width()/2,
						top:$(this).position().top - $(this).height()/2		
					}); 
				});
			}
		}
	}
	
	function initMaps(){
		
		if ( ! document.getElementById("google-map") ) return;
		
		
		var ip = document.title.indexOf('Ipanema') != -1;
		
		
		var ipanema = new google.maps.LatLng(-22.985482, -43.207283);
		var arpoador = new google.maps.LatLng(-22.988501, -43.191807);
		var latlang = ip ? ipanema : arpoador;
		var zoom = ip ? 14 : 15;
		
		
		var mapOptions = {
		  center: latlang,
		  zoom: zoom,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		  
		  ,mapTypeControl: false
		};
		var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);
			
		var styles = [
		  {
			stylers: [
			  { saturation: -70 }
			]
		  }
		];
		map.setOptions({styles: styles});
	
	
		
		var beachMarker = new google.maps.Marker({
		  position: latlang,
		  map: map,
		  icon: TEMPLATE_URL + '/imgs/mapicon.png'
	  });
	  
	}
    </script>

   >

<?php wp_footer(); ?>
<div class="bg-modal" style="
	display: none;">
	<div class="box">
		<img src="<?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail.png"
     srcset="<?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail@2x.png 2x, 
             <?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail@3x.png 3x"
     class="hotel-arpoador-gmail">
     <span class="line"></span>
     <div class="Caro-hspede-Sua-re">
     	Caro hóspede,
		<br>
		<br>
		Sua reserva está sendo redirecionada para o hotel IPANEMA INN, o hotel boutique mais querido de Ipanema. O Hotel Arpoador irá reabrir as suas portas em Dezembro de 2017, totalmente renovado, repleto de novidades e serviços exclusivos.
     </div>
     <div class="Por-favor-aguarde">
     	<img src="<?php echo get_template_directory_uri() ?>/imgs/modal/load.gif"
     class="Fill-15">Por favor, aguarde. Você está sendo redirecionado.
     </div>
	</div>
</div>


<div class="bg-modal-en " style="
	display: none;">
	<div class="box">
		<img src="<?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail.png"
     srcset="<?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail@2x.png 2x, 
             <?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail@3x.png 3x"
     class="hotel-arpoador-gmail">
     <span class="line"></span>
     <div class="Caro-hspede-Sua-re">
     	Dear guest, 
		<br>
		<br>
		Your reservation is being redirected to the IPANEMA INN, the most sought after boutique hotel in Ipanema, Rio de Janeiro. The HOTEL ARPOADOR reopens its doors on December 2017, fully renovated and with myriad of novelties and exclusive services
     </div>
     <div class="Por-favor-aguarde">
     	<img src="<?php echo get_template_directory_uri() ?>/imgs/modal/load.gif"
     class="Fill-15">
		Please wait. You are being redirected.
     </div>
	</div>
</div>
</body>
</html>