
<?php $language = $_GET['language']; ?>
<form action="#" class="f-booking" data-behavior="default_values datepicker booking" data-url="https://gc.synxis.com/">
    <input type="hidden" id="hotel" name="Hotel" value="">
    <input type="hidden" id="chain" name="Chain" value="">
    <ul>
        <li>
            <input type="text" class="input-date hasDatepicker" id="from" name="arrive" value="<?php echo $language == 'en' ? 'check-in' : 'check-in';?>" data-default="<?php echo $language == 'en' ? 'check-in' : 'check-in';?>">
        </li>
        <li>
            <input type="text" class="input-date hasDatepicker" id="to" name="depart" value="<?php echo $language == 'en' ? 'check-out' : 'check-out';?>" data-default="<?php echo $language == 'en' ? 'check-out' : 'check-out';?>">
        </li>
        
        <li>
            <div class="selector" id="uniform-adult"><span><?php echo $language == 'en' ? 'adults' : 'adultos';?></span><select id="adult" name="adult" data-behavior="uniform" style="opacity: 0;">
                <option value="">Number of adults</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>                
            </select></div>
        </li>
        <li>
            <!-- the bt is deactivated so it send a basic mailto - to activate it remove the class "deactivated" + set the url as "#" -->
            <a href="#" class="bt" data-behavior="popup"><?php echo $language == 'en' ? 'Book Now ' : 'Reservar Agora ';?>→</a>
        </li>
    </ul>
</form>   
<div class="book-now-mobile btn"><?php echo $language == 'en' ? 'Book Now →' : 'Reservar Agora →'; ?></div>