<?php
/**
 * Template Name: City & Sea Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>


<div id="main" class="cityandsea-page">    

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar(); ?>
    </div>
	
	<div id="page-wrapper" class="margin-wide bgyellow">

	<section>
        <?php  $header = get_field( 'pagina_de_cabecalho', 'options' ); ?>
        <h1 class="tcenter"><div id="logo-cityandsea"></div><?php echo $header->post_title; ?></h1>
        <div class="tcenter subtitle"><?php echo apply_filters('the_content', $header->post_content); ?></div>
        
        <div id="slide-post">
        	<div class="border-wide-slide"></div>
            
            <div id="slide-img" class="slides">
                <?php 
                    $highlights = get_field('highlights', $header->ID);
                    foreach( $highlights as $highlight): 
                    $highlights_ids[] = $highlight -> ID;
                    $img = get_field('image', $highlight -> ID );
                    echo '<img src="'. $img['sizes']['medium-slide'] .'"/>';
                    endforeach;
                ?>
            </div>
            <div id="slide-txt">
            <ul>
            <?php 
                foreach($highlights as $highlight): 
                    $post_id = $highlight -> ID;
                    $category = get_the_category($post_id);            
            ?>
                    <li>
                        <p class="cat"><a href="<?php echo get_category_link( $category[0]->cat_ID ); ?>"><?php echo $category[0]->cat_name;?></a></p>
                        <p class="tit"><?php echo $highlight->post_title;?></p>
                        <p class="dst-italic sub-tit"><?php echo get_field( 'subtitle' , $post_id );?></p>
                        <p class="ecrp"><?php echo $highlight->post_excerpt;?></p>
                        <p><a class="more" href="<?php echo get_permalink( $post_id ); ?>"><?php the_field( 'more_text', 'option' ); ?></a><p>
                    </li>
                    <?php
                endforeach;
            ?>
            </ul>
            </div>
        </div>
        
        <div class="border-wide-slide margin-negative"></div>
        
        
        <!-- Menu Categories -->
        
        <ul id="categories">
            <?php wp_list_categories(['show_option_all' => 'All' ,'title_li' => __( '' ) ]);  ?>
        </ul>
        
        <!-- GRID POSTS -->
        <div id="grid-posts" class="row clearfix">
        <?php
            // echo '<pre>'. print_r($cat, 1) . '</pre>';            
    		$paged = isset($_POST['paged']) && $_POST['paged'] ? $_POST['paged'] : 1;
    		$args = array( 'post_type' => 'post', 'posts_per_page' => 12, 'paged' => $paged );
    		if ( $cat ) $args['category__in'] = array($cat);
    		$query = new WP_Query( $args ); if ( $query->have_posts() ): 
		?>
            <ul id="tiles">		
        		<?php while ( $query->have_posts() ):  $query->the_post(); ?>	
                    <li>
            			<div class="col4">
                        	<p class="cat"><a href="<?php $category = get_the_category(); echo get_category_link( $category[0]->cat_ID ); ?>"><?php echo $category[0]->cat_name; ?></a></p>
            				<a href="<?php the_permalink(); ?>"><img class="thumb-article" src="<?php $img = get_field('image'); echo $img['sizes']['thumbnail-grid-4']; ?>" /></a>
            				<a href="<?php the_permalink(); ?>"><p class="tit"><?php the_title(); ?></p></a>
                            <p class="dst-italic"><?php $date = get_field('date'); echo $date; ?></p>
                            <!--<a href="<?php the_permalink(); ?>" class="none"><p class="ecrp"><?php the_excerpt(); ?></p></a>-->
                            <p class="ecrp"><?php echo wp_trim_words( get_the_content(), $num_words = 30, '...<a href="'. get_permalink() .'" class="more blocky"> ' . (get_field('read_more_text', 'option')) . '</a>' ); ?></p>
                        </div>
                    </li>
        		<?php endwhile;  $max_pages = $query->max_num_pages; ?>
            </ul>
        <?php endif; ?>
        
        </div>
		
        <!-- Pagination -->
        <?php if($max_pages>1): ?>
            <ul id="pagination" class="clearfix">
                <?php if ( $paged > 1 ): ?>
                <li>
                    <form method="post">
                        <input type="hidden" name="paged" id="paged" value="<?php echo ( $paged - 1 ); ?>" />
                        <a class="pagination-links" href="#"><?php the_field( 'eventos_anteriores', 'option' );?></a>
                    </form> 
                </li> 
            <?php
            endif;
            // next
            if ( $paged < $max_pages ): ?>
                <li>
                    <form method="post">
                        <input type="hidden" name="paged" id="paged" value="<?php echo ( $query->max_num_pages + 1 - $paged ); ?>" />
                        <a class="pagination-links" href="#"><?php the_field( 'proximos_eventos', 'option' );?></a>
                    </form> 
                </li> 
            <?php endif; ?>
            </ul>
        <?php endif; ?>
        
    </section>

    </div><!-- #main-wrapper -->

<!--</div>-->

<?php get_footer(); ?>