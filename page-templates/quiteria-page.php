<?php
/**
 * Template Name: Quiteria Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>

<div id="main" class="quiteria-page"> 
    
    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
        <?php get_bar(); ?>
    </div>
    
	<?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>
    
    <section class="quiteria">
        <div class="clearfix">
            <div class="col2 col-title-quiteria">
                <div class="quiteria-about-content">
                    <img src="<?php echo get_template_directory_uri() . '/imgs/logo-quiteria.png' ?>" alt="" />                
                </div>
            </div>
            <div class="col1 col-address">
            	Rua Maria Quitéria 27<br />
                Ipanema<br />
                Rio de Janeiro<br />
                <span class="pin-map">
                    <img src="<?php echo get_template_directory_uri() . '/imgs/pin-map.png' ?>" alt="" /> <a href="https://www.google.com.br/maps/place/R.+Maria+Quit%C3%A9ria,+27+-+Ipanema,+Rio+de+Janeiro+-+RJ,+22410-040/@-22.9854468,-43.2071746,17z/data=!3m1!4b1!4m2!3m1!1s0x9bd50441a77ea9:0x79a7f490a6a52fd8" target="_blank"><?php echo $language == 'en' ? 'MAP' : 'MAPA'?> →</a>
                </span>
                <br />
                <strong class="text-reservas"><?php echo $language == 'en' ? 'RESERVE' : 'RESERVAS'?></strong><br />
                +55 21 2267 4603<br />
                <a href="mailto:quiteria@ipanemainn.com.br">quiteria@ipanemainn.com.br</a>
			</div>
            <div class="col2">
                
                <div class="quiteria-about-content">
                	<p style="line-height: 1.54em;">
                    <?php echo $language == 'en' ? 'Located on Rua Maria Quitéria, a shaded, tree-lined street running perpendicular to Ipanema beach, QUITÉRIA specializes in fresh, local and seasonal ingredients carefully picked and prepared by Chef Christian Garcia to meet the highest standards.  The relaxed ambiance was designed by renowned local architect Bel Lobo and her team and refers back to the period of Brazilian Modernism, with its confortable chairs by the award-winning Brazilian designer Sérgio Rodrigues.  The installation created by visual artist Gisele Camargo leads to an open kitchen where diners can enjoy watching the dishes finalized in this unique experience.' : 
					'Localizado na frondosa Rua Maria Quitéria, transversal à praia de Ipanema, o restaurante é especializado em ingredientes frescos, locais e sazonais, cuidadosamente escolhidos e preparados pelo Chef Christian Garcia no mais alto padrão. A atmosfera relax do restaurante, que foi projetado pela renomada arquiteta local Bel Lobo e pelo seu estúdio, é uma referência à era do modernismo brasileiro, com cadeiras confortáveis pelo premiado designer brasileiro Sérgio Rodrigues. A instalação de arte criada pela artista visual Gisele Camargo chama a atenção para a cozinha aberta, onde os pratos são finalizados. É uma experiência única.</p>'; ?>
				</div>
            </div>
        </div>
        <div class="clearfix">
            <div id="chef">
                <div class="restaurant">
                    <p class="time">
                    <?php echo $language == 'en' ? 'Open every day' : 'Aberto todos os dias'; ?><br />
                    <?php echo $language == 'en' ? 'from 7am to midnight' : 'de 7h a meia-noite'; ?><br /><br />

                    <span class="sorcesanssemibold"><?php echo $language == 'en' ? 'Breakfast' : 'Café'; ?></span><?php echo $language == 'en' ? ' 7am - 11am' : ' 7h - 11h '; ?><br />
                    <span class="sorcesanssemibold"><?php echo $language == 'en' ? 'Lunch' : 'Almoço'; ?></span><?php echo $language == 'en' ? ' 12pm - 5pm' : ' 12h - 17h '; ?><br />
                    <span class="sorcesanssemibold"><?php echo $language == 'en' ? 'Dinner' : 'Jantar'; ?></span><?php echo $language == 'en' ? ' 6pm - midinight' : ' 18h - 24h '; ?>
                    </p>
                </div>
            	<div class="main-chef">
                    <div class="chef">
                        <img src="<?php echo get_template_directory_uri() . '/imgs/chef-christian-garcia.jpg' ?>" alt="" />
                        <br />
    				    <strong class="title">Chef  Christian Garcia</strong>
                	   <p class="chef-bio">
                        <?php echo $language == 'en' ? 'Originally from Argentina, chef Christian Garcia grew up between Argentina, Spain and Italy.  Christian graduated from the Unión de Chefs Argentinos and has worked in the Spanish cities of Valencia, Barcelona and Andorra.  More recently, he ventured around the Middle East and Africa where he worked in places like Saudi Arabia, Dubai and Zanzibar, and was nominated the best chef in Saudi Arabia by the much acclaimed “Chaîne des Rotisseurs.” Christian brings his vast experience in Mediterranean and Asian cuisines to perfect his use of Brazilian ingredients, which he prepares using international techniques and serves in a contemporary style.' : 
        				'De origem argentina, Christian Garcia cresceu entre Argentina, Espanha e Itália. É formado pela União de Chefs Argentinos e trabalhou em cidades espanholas como Valência, Barcelona e Andorra. Mais recentemente embarcou para o Oriente Médio e se aventurou pela Arábia Saudita e Dubai, além de Zanzibar, na África. Christian foi escolhido como melhor chef da Arábia Saudita pelo aclamado “Chaine des Rotisseurs”. Conta com vasta experiência nas cozinhas mediterrânea e asiática e hoje foca sua pesquisa nos ingredientes brasileiros feitos através de técnicas mundiais e servidos de forma contemporânea.'; ?>
                        </p>
                    </div>
                </div>
        	</div>
        </div>
            <div class="clearfix main-meal">
            	<div class="photos">
                	<img src="<?php echo get_template_directory_uri() . '/imgs/chef-1.jpg' ?>" alt="" style="margin-bottom:20px; display:block; float:right" />
                	<img src="<?php echo get_template_directory_uri() . '/imgs/chef-2.jpg' ?>" alt="" style="display:block; float:right;" />
                </div>
            	<div class="about-menu">
                	<img src="<?php echo get_template_directory_uri() . '/imgs/bar.jpg' ?>" alt="" style="margin-bottom:10px; display:block;" />                	
                    <strong>Drinks</strong>
					<p>
					<?php echo $language == 'en' ? 'QUITÉRIA offers a concise yet sophisticated wine list, broken down by types of grape to pair harmoniously with your choice of dish.  Enjoy the classic cocktails prepared by our bartenders, true Caipirinha masters who serve from the classic to their own signature versions, always using the best seasonal fresh fruits available.' : 
                	'O QUITÉRIA oferece uma carta de vinhos concisa, porém sofisticada, dividida por tipos de uvas  que irá combinar com a sua escolha de pratos de forma harmoniosa. Você poderá desfrutar cocktails clássicos preparados pelos nossos bartenders, que são mestres em caipirinhas clássicas e de assinatura, todas feitas com frutas frescas desta temporada.'; ?>
                    </p>
				</div>
                <div class="photos desktop">
                    <img src="<?php echo get_template_directory_uri() . '/imgs/chef-1.jpg' ?>" alt="" style="margin-bottom:20px; display:block; float:right" />
                    <img src="<?php echo get_template_directory_uri() . '/imgs/chef-2.jpg' ?>" alt="" style="display:block; float:right;" />
                </div>
            </div>
            
        </div>
        
    </section>
    <?php endwhile; // end of the loop. ?>
</div>
<!-- RESTORANDO Widget Start -->
<link href="<?php echo site_url(); ?>/wp-content/restorando/restorando.css" rel="stylesheet" type="text/css" /> 
<script src="http://mkt.restorando.com/widget/restorando.js" type="text/javascript"></script> 
<div><a href="javascript:void(0);" onClick="parent.abrirFrame(320,490,'../wp-content/restorando/reservas.html');" class="tab_reservas">RESERVAS</a></div>
<!-- RESTORANDO Widget End -->
<!-- #main --> 

<!-- ###################      get_footer    ##################### -->

<div class="clearfix"></div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZjBxFScnY08Kd9TWJx1C57JSVkq-06Kk&sensor=false"></script> 
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery-1.10.1.min.js'; ?>"><\/script>')</script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.easing.1.3.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/slidesjs/jquery.slides.min.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.imagesloaded.min.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.transit-master/jquery.transit.min.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/wookmark/jquery.wookmark.min.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.instagram/jquery.instagram.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/twitter/twitterFetcher_v10_min.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/vendor/glDatePicker-2.0/glDatePicker.js'; ?>"></script> 
<script src="<?php echo get_template_directory_uri() . '/js/main.js'; ?>"></script> 

</body>
</html>