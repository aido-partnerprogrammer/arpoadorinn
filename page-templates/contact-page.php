<?php
/**
 * Template Name: Contact Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>


<div id="main" class="contact-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar(); ?>
    </div>

	<div id="page-wrapper" class="margin-wide">



<?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>
	
    <section>
    	
        <!-- PAGE TITLE      -->
        <h1 class="tcenter"><?php the_title(); ?></h1>

		<div class="row clearfix">
        	<div class="col3">
            </div>
        	<div class="col3">
            	<hr/>
            </div>
        	<div class="col3">
            </div>
        </div>
        
        <div class="row clearfix">
            <div class="col3 directions">
                <?php echo apply_filters('the_content', get_field( 'endereco' )); ?>
            </div>
            <div class="col-form">
                <?php echo do_shortcode(get_field('shortcode_formulario')) ?>
            </div>
        </div>
        <script>document.addEventListener( 'wpcf7mailsent', function( event ) {location = '<?php the_field( 'pagina_de_sucesso' ); ?>';}, false );</script>
        
        <!-- <form action="<?php the_field( 'action_formulario' ); ?>" method="POST">
        <input type="hidden" name="recipient" value="<?php the_field( 'email_destinatario' ); ?>">
		<input type="hidden" name="redirect" value="<?php the_field( 'pagina_de_sucesso' ); ?>">       <input type="hidden" name="subject" value="Contato Site <?php echo ICL_LANGUAGE_NAME; ?>">
		<div class="row clearfix">
        	<div class="col3 directions">            	
                <?php echo apply_filters('the_content', get_field( 'endereco' )); ?>
            </div>
        	<div class="col3">
                <div class="input">
                    <p><?php the_field( 'label_nome' ); ?>*</p>
                    <input name="Nome" type="text"/>
                </div>	
                <div class="input">
                    <p><?php the_field( 'label_email' ); ?>*</p>
                    <input name="email" type="text"/>
                </div>	
                <div class="input">
                    <p><?php the_field( 'label_phone' ); ?></p>
                    <input name="Phone" type="text"/>
                </div>	
            </div>
        	<div class="col3">
                <div class="input">
                    <p><?php the_field( 'label_mensagem' ); ?>*</p>
                    <textarea name="text"></textarea>
                </div>
                <input name="Submit" type="submit" value="<?php the_field( 'label_enviar' ); ?>" class="btn">
            </div>
        </div>
		</form> -->
        
        <div class="spacer60"></div>
        
        <!-- HOW TO GET HERE -->
        <hr/>
        <!-- block subtitle -->
        <h2 class="tcenter"><span><?php the_field('title'); ?></span></h2>
        <p class="tcenter subtitle"><?php the_field('text'); ?></p> 
        <div class="spacer60"></div> 
        
       <!-- MAP MODULE, to include in HOME !!!! -->
        <center>
            <div id="container-map">
                <?php $image = get_field('image_map', $id ); 
                    echo '<img src="' . $image['sizes']['large'] . '" class="wideimage" id="map">'; 
                    $coor = preg_split( '/,/' , get_field('hotel_position', $id ) );
                    $imghotel = get_field('image_hotel', $id );
                    echo '<div class="map-marker-big marker" style="left:'. $coor[0] .'px;top:'. $coor[1] .'px;position:absolute;" data-rel="' . $coor[0] . ',' . $coor[1] . '" data-img="' . $imghotel['sizes']['map-window'] . '"></div>';
                    $columns = get_field('columns', $id );
                    $i = 0;
                    foreach ( $columns as $column ):
                        $indications = $column['indication'];
                        foreach ( $indications as $indication ):
                            $coor = preg_split( '/,/', $indication['position_inside_the_map'] );
                            echo '<div rel="'. $i .'" class="map-marker marker" style="left:'. $coor[0] .'px;top:'. $coor[1] .'px;position:absolute;"  data-rel="' . $coor[0] . ',' . $coor[1] . '" data-img="' . $indication['image']['sizes']['map-window'] . '"></div>';
                            $i++;
                        endforeach;											
                    endforeach;
                ?>
                <div id="win-marker"></div>
            </div>
        </center>	
       
        <div id="content-info-map">
            <h3 class="contact-link"><?php echo get_field('google_maps_link'); ?></h3>
            <!-- Indications -->
            <div class="spacer60"></div> 
            <div class="row clearfix" id="info-map">
            <?php 
                $columns = get_field('columns', $id);
                $i = 0;
                foreach ( $columns as $column ):
            ?>
                <div class="col3">
                <?php 
                    $indications = $column['indication'];
                    foreach ( $indications as $indication ):
                ?>
                    <div class="indication" rel="<?php echo $i; ?>">
                        <div class="i-title"><?php echo $indication['title']; ?></div>
                        <div class="i-text" ><?php echo $indication['text']; ?></div>
                    </div>
                 <?php
                    $i++;
                    endforeach;
                 ?>
                </div>
             <?php
                endforeach;
             ?>
            </div>
        </div>
        <!-- MAP !!!! -->	
        
    </section>

<?php endwhile; // end of the loop. ?>
    </div><!-- #main-wrapper -->

<?php get_footer(); ?>