<?php
/**
 * Template Name: Contact Page Send Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>

<div id="main" class="contact-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar(); ?>
    </div>

	<div id="page-wrapper" class="margin-wide">

	<?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>
	
        <section>
        <!-- PAGE TITLE      -->
        <h1 class="tcenter"><?php the_title(); ?></h1>
        
        <div class="row clearfix">
        	<div class="col3">
            </div>
        	<div class="col3">
            	<p class="tcenter subtitle"><?php the_content(); ?></p>
				<div class="spacer60"></div>
            </div>
        	<div class="col3">
            </div>
        </div>
        
       
       
    </section>            
            
<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>