<?php
/**
 * Template Name: Package Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>


<div id="main" class="packages-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar(); ?>
    </div>

	<div id="page-wrapper" class="margin-wide">


<?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>


	<section>
        <h1 class="tcenter"><?php the_title(); ?></h1>		
        <div id="nav-rooms-header" class="margin-wide">
            <div class="row clearfix">
                <div class="col3">
                </div>
                <div class="col3" id="menu-limit">
                    <hr/>
                </div>
                <div class="col3">
                </div>
            </div>
        </div>
        
        <div class="row clearfix">
        <?php 
		$packages = get_field('packages');
		$layout = array();
		foreach ( $packages as $package ):
			if ( $package['active'] ) $layout[] = true;
		endforeach;
		switch (count( $layout )){
			case 3:
				// 2 columns, 1 big, one short
				$big = 0;
				if ( isset($_GET['more']) && $_GET['more'] ) $big = $_GET['more'];
				$package = $packages[$big];
				$image = $package['image'];
				?>
                <div class="col3_2">
                	<img src="<?php echo $image['sizes']['medium']; ?>"/>
                    <h2><?php echo $package['title']; ?></h2>
                    <h3><?php echo $package['advertistment']; ?></h3>
                    <p class="dst-italic"><?php echo $package['date']; ?></p>
                    <p><?php echo $package['excerpt']; ?></p>
                    <!-- book btn -->
                    <div class="btn book-now-btn"><?php echo $language == 'en' ? 'Book Now →' : 'Reservar Agora →'; ?></div>
                    <table class="prices">
                    <?php 
					$prices = $package['prices'];					
					foreach ( $prices as $price ):
						$room = $price['room'];
						//print_r( $room );
						echo '<tr><td class="ssb15"><a href="' . get_permalink( $room -> ID ) . '">' . ($room -> post_title) . '</a></td><td>' . $price['price_column_1'] . '</td><td>' . ($price['price_column_2'] != '' ? $price['price_column_2'] : '')  . '</td></tr>';
					endforeach;
					?>
                    </table>
                    <div><?php echo $package['taxes']; ?></div>
                    <p>
                    	<h6><?php echo $language == 'en' ? 'Conditions:' : 'Condições:'; ?></h6>
						<?php echo $package['conditions']; ?>
                    </p>
                </div>
                <div class="col3">
                <?php
				$i = 0;
				foreach ( $packages as $package ):
					if ( $big != $i ) :
					$image = $package['image'];
					?>
					<a href="<?php echo get_permalink(). '?more=' . $i ;?>"><img src="<?php echo $image['sizes']['thumbnail']; ?>"/></a>
                    <h2><a href="<?php echo get_permalink(). '?more=' . $i ;?>"><?php echo $package['title']; ?></a></h2>
                    <h3><?php echo $package['advertistment']; ?></h3>
                    <p class="dst-italic"><?php echo $package['date']; ?></p>
                    <p><?php echo $package['excerpt']; ?></p>
                    <p><a class="more" href="<?php echo get_permalink(). '?more=' . $i ;?>"><?php the_field( 'mais_sobre', 'options' ); ?></a></p>
                    <!-- book btn -->
                    <div class="btn book-now-btn"><?php the_field( 'texto_botao', 'options' ); ?></div>
                    <div class="spacer60"></div>
					<?php
					endif;
					$i++;
				endforeach;
				?>
                </div>
                <?php
			break;
			default:
				// 2 columns, middle ...
				foreach ( $packages as $package ):
				if ( $package['active'] ):
				$image = $package['image'];
				?>
                <div class="col1_2">
                	<img src="<?php echo $image['sizes']['medium']; ?>"/>
                    <h2><?php echo $package['title']; ?></h2>
                    <h3><?php echo $package['advertistment']; ?></h3>
                    <p class="dst-italic"><?php echo $package['date']; ?></p>
                    <p><?php echo $package['excerpt']; ?></p>
                    <!-- book btn -->
                    <div class="btn book-now-btn"><?php the_field( 'texto_botao', 'options' ); ?></div>
                    <table class="prices">
                    <?php 
					$prices = $package['prices'];					
					foreach ( $prices as $price ):
						$room = $price['room'];
						//print_r( $room );
						echo '<tr><td class="ssb15"><a href="' . get_permalink( $room -> ID ) . '">' . ($room -> post_title) . '</a></td><td>' . $price['price_column_1'] . '</td><td>' . ($price['price_column_2'] != '' ? $price['price_column_2'] : '')  . '</td></tr>';
					endforeach;
					?>
                    </table>
                    <div><?php echo $package['taxes']; ?></div>
                    <p>
                    	<h6><?php echo $language == 'en' ? 'Conditions:' : 'Condições:'; ?></h6>
						<?php echo $package['conditions']; ?>
                    </p>
                </div>
                <?php
				endif;
				endforeach;
		};
		?>
        </div>
              
    </section>

<?php endwhile; // end of the loop. ?>
    </div><!-- #main-wrapper -->

<?php get_footer(); ?>