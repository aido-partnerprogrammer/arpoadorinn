<?php
/**
 * Template Name: Guests Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=365175853494947";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="main" class="guests-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar(); ?>
    </div>

	<div id="page-wrapper" class="margin-wide">


<?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>


	<section>	
        <h1 class="tcenter"><?php the_field('title'); ?></h1>
   		<div class="tcenter subtitle"><?php echo the_excerpt(); ?></div>
		<div class="row clearfix">
        	<div class="col3">
            </div>
        	<div class="col3">
            	<hr/>
            </div>
        	<div class="col3">
            </div>
        </div>
        
        <?php
		$socials = get_field('social_links', 'options');
		$twitter = $socials[1];
		$instagram = $socials[0];
		?>
        <ul id="social">
        	<li>
				<a href="<?php echo $instagram['link']; ?>"><div class="icon-insta"></div></a>
            </li>
            <li>
<!-- twitter follow -->
<a href="<?php echo $twitter['link']; ?>" class="twitter-follow-button" data-show-count="false" data-lang="en" data-size="">follow @<?php echo $twitter['user']; ?></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

			</li>            
            <li>
<!--  Like Button -->           
            
				<div class="fb-like" data-href="<?php echo get_bloginfo('url'); ?>" data-show-faces="false" data-send="false" data-layout="button_count"></div>			
			</li>            
        </ul>
        
        
		<div class="row clearfix">
        	<div class="col1_2">
            	<div class="innercol">
                	<h2><span><?php the_field('title_photo');?></span></h2>
                    <div class="dst-italic point16"><?php the_field('text_photo');?></div>
                    
                    <!-- user photos -->
                    <div id="slide-img" class="slides">
                    <?php 
						$highlights = get_field('guest_gallery', 'options');
						$highlights = array_reverse($highlights);
                        foreach( $highlights as $highlight): 
                        $img = $highlight['image'];
                        echo '<img src="'. $img['sizes']['medium-slide'] .'"/>';
                        endforeach;
                    ?>
                    </div>
                    <div id="slide-txt">
                    <div class="cap">“</div>
                    <ul>
                    <?php 
                        foreach( $highlights as $highlight): 
                            ?>
                            <li>
                                <div class="dsct-italic"><?php echo $highlight['text'];?></div>
                                <div class="name">— <?php echo $highlight['name'];?></div>
                            </li>
                            <?php
                        endforeach;
                    ?>
                    </ul>
                    </div>
                    
                    <div id="insta-container">
                        <h2><span>INSTAGRAM</span></h2>
                    	<div id="instagram" class="loading"></div>
                    </div>
                    <div class="spacer40"></div>
                    <div><a class="more instamore" href="#"><?php the_field( 'mais_sobre', 'options' ); ?> Instagram →</a></div>
                </div>	
            </div>
        	<div class="col1_2">
            	<div class="innercol">
                	<h2><span><?php the_field('text_review');?></span></h2>
                    
                    <!-- Trip Advisor -->
                    
                    <div class="t-comments">
						<?php
                        $comments = get_field( 'comments');
                        foreach ( $comments as $comment ):
                        ?>
                        <div class="t-comment">
                            <div class="dsct-italic title"><?php echo $comment['title'];?></div>
                            <div class=""><?php echo $comment['text'];?></div>
                            <div class="name"><div class="t-log"></div> — <?php echo $comment['name'];?>
                            <?php
                                $stars = (int)$comment['stars'];
                                if ( $stars > 0 ):
                                    ?>
                                    <span class="stars">
                                    <?php
                                    for ( $i=0; $i<$stars; $i++ ):
                                        echo '<div class="star"></div>';
                                    endfor;
                                    ?>
                                    </span>
                                    <?php
                                endif; 
                            ?>
                            </div>
                        </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                    <div class="more-trip"><a class="more link-tripadvisor" href="<?php echo get_field('link_more_review'); ?>"><div class="ta-logo"></div><?php echo get_field('text_more_review'); ?></a></div>

                    <hr />
					
                    <!-- Tweets -->
                    
                	<h2><span><?php the_field('title_tweet');?></span></h2>
                    <div id="tweets" class="loading"></div>
                    <p><a class="more" href="<?php echo $twitter['link']; ?>"><?php the_field( 'mais_sobre', 'options' ); ?> Twitter →</a><p>


                </div>	
            </div>
        </div>                
                
    </section>

<?php endwhile; // end of the loop. ?>
    </div><!-- #main-wrapper -->

<?php get_footer(); ?>