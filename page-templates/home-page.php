<?php
/**
 * Template Name: Home Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); 
?>


<div id="home-header" >
     <div class="margin-wide">
     	<div class="ext-wrap">
            <!-- logo -->        
            <a href="<?php bloginfo( 'url' ); ?>" class="home-link"><div id="logo"></div></a>            
            <!-- link other hotel only in home -->
            <div id="link-hotel" class="link-other-hotel">
                <a class="dst-italic" href="<? echo get_field('link_hotel', 'options');?>"><? echo get_field( 'visit_text', 'options' ); ?> <span class="arrow"></span>  <span><? echo get_field('hotel_name', 'options'); ?></span></a>
            </div>
            <!-- language selector -->
            <?php layout_langswitch('lang-switch-home'); ?>    
        </div>
    </div>
</div>

<div id="main" class="margin-wide">

    <!-- booking bar -->
    <div id="book-now-bar" style="display: none;">  
    	<?php get_bar();?>
    </div>

    <!-- SLIDESHOW HOME -->
    <div class="slides">
        <?php 
            $images = get_field('gallery', 'options' );
            foreach ( $images as $image ):
                echo '<img src="'. $image['url'] .'">';
            endforeach;
        ?>
    </div>   

	<div id="home-content" class="ext-wrap">
	<?php 
        $blocks = get_field('block');
        // echo '<pre>'.print_r($blocks,1). '</pre>';
        // die();          
        foreach ( $blocks as $block ):
            $relation = $block['relation'];
            $title_page = $relation -> post_title;
            $page = get_page_by_title( $title_page );
            $page_id = $page->ID;
    		$page_link = get_permalink( $page_id ); 
            $layout = $block['layout'];
            // echo $layout;
    ?>
        <section>
        <?php
		
		//echo $title_page;
		
        switch ($layout){
            case ('Rooms') :            
            	?>
                <div class="">
                    <h1 class="tcenter"><?php echo $block['title']; ?></h1>
                </div>
                <?php if ( have_rows('home_slideshow', 'option') ) : ?>
                    <div class="row clearfix">
                        <div class="col12">
                        <div class="slideshow">
                        <?php while( have_rows('home_slideshow', 'option') ) : the_row(); ?>
                            <div>
                            <a href="<?php echo get_sub_field('link_slider_home')['url']; ?>" target="<?php echo get_sub_field('link_slider_home')['target']; ?>" class="box-slider">
                                <img src="<?php echo get_sub_field('image_slider_home')['sizes']['sllider-thumb']; ?>" class="img-responsive" alt="<?php echo get_sub_field('link_slider_home')['title']; ?>">
                                <span class="mask"><?php echo get_sub_field('link_slider_home')['title']; ?></span>
                            </a>
                            </div>
                        <?php endwhile; ?>
                        </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="tcenter btlink"><a href="<?php echo $page_link; ?>"><?php echo $block['texto_veja_mais'] . '<span>'.$block['texto_veja_mais_bold'].'</span>'; ?></a></div>
                <?php
            break;
            case ( 'Contact' ) :
				$image = get_field('image_map', $page_id);
                // echo '<pre>'. print_r($block, 1) . '</pre>';
                
            	?>
                
                </section>
                </div>
                <section class="contact-home" style="background:#bdebed;">
                
                
                <div>
                    <h1 class="tcenter"><?php echo $block['title']; ?></h1>
                    <div class="tcenter subtitle"><?php echo $block['text']; ?></div>
                </div>
                <div id="bg-map">
                	<!--<center><a href="<?php echo $page_link; ?>"><img src="<?php echo $image['sizes']['large'];?>"></a></center>-->
                    <?php $id = $page_id; ?>
                    
                       <!-- MAP MODULE, to include in HOME !!!! -->
                        <center>
                            <div id="container-map">
                                <?php $image = get_field('image_map', $id ); 
                                    echo '<img src="' . $image['sizes']['large'] . '" class="wideimage" id="map">'; 
                                    $coor = preg_split( '/,/' , get_field('hotel_position', $id ) );
                                    $imghotel = get_field('image_hotel', $id );
                                    echo '<div class="map-marker-big marker" style="left:'. $coor[0] .'px;top:'. $coor[1] .'px;position:absolute;" data-rel="' . $coor[0] . ',' . $coor[1] . '" data-img="' . $imghotel['sizes']['map-window'] . '"></div>';
                                    $columns = get_field('columns', $id );
                                    $i = 0;
                                    foreach ( $columns as $column ):
                                        $indications = $column['indication'];
                                        foreach ( $indications as $indication ):
                                            $coor = preg_split( '/,/', $indication['position_inside_the_map'] );
                                            echo '<div rel="'. $i .'" class="map-marker marker" style="left:'. $coor[0] .'px;top:'. $coor[1] .'px;position:absolute;"  data-rel="' . $coor[0] . ',' . $coor[1] . '" data-img="' . $indication['image']['sizes']['map-window'] . '"></div>';
                                            $i++;
                                        endforeach;											
                                    endforeach;
                                ?>
                                <div id="win-marker"></div>
                            </div>
                        </center>	
                       
                        <div id="content-info-map">
                            <h3 class="contact-link"><?php echo get_field('google_maps_link'); ?></h3>
                            <!-- Indications -->
                            <div class="spacer60"></div> 
                            <div class="row clearfix" id="info-map">
                            <?php 
                                $columns = get_field('columns', $id);
                                $i = 0;
                                foreach ( $columns as $column ):
                            ?>
                                <div class="col3">
                                <?php 
                                    $indications = $column['indication'];
                                    foreach ( $indications as $indication ):
                                ?>
                                    <div class="indication" rel="<?php echo $i; ?>">
                                        <div class="i-title"><?php echo $indication['title']; ?></div>
                                        <div class="i-text" ><?php echo $indication['text']; ?></div>
                                    </div>
                                 <?php
                                    $i++;
                                    endforeach;
                                 ?>
                                </div>
                             <?php
                                endforeach;
                             ?>
                            </div>
                        </div>
                        <!-- MAP !!!! -->	
                    
                    
                </div>
                <div style="background:#bdebed;padding-bottom:10px;padding-top:20px;">
                	<h3 class="tcenter"><?php echo get_field('google_maps_link', $page_id ); ?></h3>
                    
                	<div class="tcenter btlink"><a href="<?php echo $page_link; ?>"><?php echo $block['texto_veja_mais'] . '<span>'.$block['texto_veja_mais_bold'].'</span>'; ?></a></div>
                </div>
                
                </section>
                <div class="ext-wrap">
                
                <?php
            break;

            /*
            case ('Here with you') :
            	?>
                <div>
                    <h1 class="tcenter"><?php echo $block['title']; ?></h1>
                	<div class="subheader tcenter subtitle">
                    	<?php echo get_field( 'text_photo', $page_id ); ?>
                        <div class="tcenter t-link"><ul><li><a class="more" target="_blank" href="<?php echo get_field('link_more_review', $page_id); ?>"><?php the_field( 'review_tripadvisor', 'options' ); ?></a></li><li><a class="more" target="_blank" href="<?php echo get_field('link_more_review', $page_id); ?>"><div class="tlogop"></div></a></li></ul></div>
                    </div>
                </div>
                <div class="grid-rooms guests-home">
                	<div class="row clearfix">
                    	<div class="col2">
						<?php 
                            $p_users = get_field('guest_gallery', 'options' );
							$comments = get_field( 'comments', $page_id );
                            $comments = array_reverse($comments);
							shuffle( $p_users );
							$j = 0;
							$n = 0;
                            for($i = 0; $i < 5; $i++):
								$img = ''; 
                                //$img = $i == 2 ? '<div class="loading placeholder"></div>' : '<div class="loading placeholder"><div class="loader"></div><div class="photo-txt"><div class="cap">“</div><div class="dsct-italic">'+$comments[0]['title']+'</div><div class="name">'+$comments[0]['title']+'</div></div></div>';
                                switch ( $i){
                                    case 0 :
										$comment = $comments[$n];
										$n++;
										$img =  $comment ? '<div class="photo-txt"><div class="cap">“</div><div class="dsct-italic">'.$comment['title'].'</div><div class="name"><div class="t-log"></div> — <span>'.$comment['name'].'</span></div></div>' : '';
                                        $img .= '<img  src="' . $p_users[$j]['image']['sizes']['medium'] . '" >';
										$j++;						
                                    break;
									case 1:
										$comment = $comments[$n];
										$n++;
										$img =  $comment ? '<div class="loading placeholder"><div class="loader"></div></div><div class="photo-txt"><div class="cap">“</div><div class="dsct-italic">'.$comment['title'].'</div><div class="name"><div class="t-log"></div> — '.$comment['name'].'</div></div>' : '';
									break;
									case 2:
										$img = '<div class="loading placeholder"><div class="loader"></div></div>';
									break;
                                    case 3 :
										$img = '<div class="loading placeholder"><div class="loader"></div></div>';
                                        echo '</div><div class="col1"><div class="spacer60"></div>';
                                    break;
                                    case 4 :
										$comment = $comments[$n];
										$n++;
										$img = '<img  src="' . $p_users[$j]['image']['sizes']['medium'] . '" >';
										$img .=  $comment ? '<div class="photo-txt"><div class="cap">“</div><div class="dsct-italic">'.$comment['title'].'</div><div class="name"><div class="t-log"></div> — '.$comment['name'].'</div></div>' : '';            
										$j++;						
                                    break;
                                }
                            echo '<div id="gr'. $i .'" class="' . ($i < 3 ? 'r-right' : 'r-left') . '"><a class="no-style" href="' . $page_link . '">'. $img .'</a></div>';
                            endfor;
                        ?>
                        </div>
                    </div>
                </div>
                <div class="tcenter btlink"><a href="<?php echo $page_link; ?>"><?php echo $block['texto_veja_mais'] . '<span>'.$block['texto_veja_mais_bold'].'</span>'; ?></a></a></div>
                <?php
            break;
            */

            case ('City&Sea') :
				$image = get_field('image_map', $page_id);
            	?>
                
                
                </section>
                </div>
                <section class="bgyellow city-home">
                
                <div class="ext-wrap border-top"></div>

                <div class="cs-block">
                    <h1 class="tcenter"><div id="logo-cityandsea"></div><?php echo $block['title']; ?></h1>
                    <div class="row clearfix">        
						<?php
                        $args = array( 'post_type' => 'post', 'posts_per_page' => 6 );
                        $query = new WP_Query( $args ); if ( $query->have_posts() ): 
						$i = 0;
                        while ( $query->have_posts() ): 
                            $query->the_post();
							if ( $i == 0 ||  $i == 3 ) echo '<div class="clearfix">';
                            ?>	
                            <div class="col3<?php if ( $i > 2 ) echo ' bordeup'; ?>">
                            	<?php if ( $i < 3 ) : ?>
                                <a href="<?php the_permalink(); ?>"><img src="<?php $img = get_field('image'); echo $img['sizes']['thumbnail-grid-4']; ?>" /></a>
                                <?php endif; ?>
                                <a href="<?php the_permalink(); ?>"><p class="tit"><?php the_title(); ?></p></a>
                                <p class="dst-italic"><?php $date = get_field('date'); echo $date; ?></p>
                                <p class="ecrp"><?php echo wp_trim_words( get_the_content(), $num_words = 30, '...<a href="'. get_permalink() .'" class="more blocky"> ' . (get_field('read_more_text', 'options')) . '</a>' ); ?></p>
                            </div>
                            <?php
							$i++;
							if ( $i == 3 || $i == 6 ) echo '</div>';	
                        endwhile; 
                        endif;
						wp_reset_postdata();
                        ?>
                    </div>
                    <div class="tcenter btlink"><a href="<?php echo $page_link; ?>"><?php echo $block['texto_veja_mais'] . '<span>'.$block['texto_veja_mais_bold'].'</span>'; ?></a></a></div>
                </div>
                
                
                </section>
                <div class="ext-wrap">
                <?php
            break;
            
            default :
            break;
        }
        ?>
        
        </section>
        <?php
    endforeach;
    ?>
	</div>
    
                        

<?php get_footer(); ?>