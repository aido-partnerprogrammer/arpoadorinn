<?php
/**
 * Template Name: Redirect Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */


/* Redirect to first child page */
while ( have_posts() ) : 
		the_post();
		$page_id = $post -> ID;
		$my_wp_query = new WP_Query();
		$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'order' => 'ASC' ));
		$childs = get_page_children( $page_id, $all_wp_pages );
		wp_safe_redirect( $childs[0] -> post_name ); 
endwhile; 

?>