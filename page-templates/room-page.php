<?php
/**
 * Template Name: Room Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>


<div id="main" class="rooms-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
        <?php get_bar(); ?>
    </div>

    <div id="page-wrapper" class="margin-wide">

    <?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>
    
    
        <section>
            <?php 
            /* Rooms */
            $page_parent = get_post( $post-> post_parent );
            $page_id = $page_parent -> ID;
            ?>
            <h1 class="tcenter"><?php echo $page_parent -> post_title; ?></h1>
            <div class="tcenter subtitle"><?php echo apply_filters('the_content', $page_parent -> post_content); ?></div>
            
            <div id="nav-rooms-header" class="margin-wide">
                <div class="row clearfix">
                    <div class="col3">
                    </div>
                    <div class="col3" id="menu-limit">
                        <hr/>
                    </div>
                    <div class="col3">
                    </div>
                </div>
                <nav id="nav-rooms">
                    <ul>
                    <?php wp_list_pages('title_li=&child_of=' . $page_id ); ?>
                    </ul>
                </nav>
                <div class="spacer40"></div>
            </div>
            
            <div id="slideshow" class="slides">
                <?php 
                $images = get_field('images');
                foreach ( $images as $image ):
                    echo '<img src="'. $image['sizes']['large'] .'">';
                endforeach;
                ?>
            </div>
            
            <div class="row clearfix">
                <div class="col2 border-right">
                    <h4><?php the_title(); ?></h4>
                    <div class="dst-italic"><?php the_excerpt(); ?></div>
                    <div class="ulist"><?php the_content(); ?></div>
                </div>
                <div class="col1 tcenter">
                    <span class="ssb15"><?php the_field( 'layout_da_sala', 'options' ); ?></span>
                    <?php
                        $layouts = get_field('layouts');
                        if ( count( $layouts ) > 1 ) :
                            echo '<ul id="layout">';
                            foreach ($layouts as $layout ):
                                echo '<li>' . $layout['title'] . '</li>';
                            endforeach;
                            echo '</ul>';
                        endif;
                    ?>
                    <div class="spacer60"></div>
                    <div id="layout-images">
                    <?php
                    foreach ($layouts as $layout ):
                        echo '<img src="' . $layout['image']['url'] . '">';
                    endforeach;
                    ?>
                    </div>
                </div>
            </div>            
            
            <?php
            /* Amenities */
            $title = get_field( 'title', $page_id );
            $relation = get_field( 'relation', $page_id );
            $amenities = get_field( 'amenities', $relation[0] -> ID );             
            
            if ( $amenities ) :
            $i=0;
                foreach ( $amenities as $amenity ):
                    
                    $blocks = $amenity['amenities_block'];
                    $cssClass = $i == 0 ? 'servicos-complementares' : 'nossos-parceiros';

                    if ( $blocks ):                
                    if ( $i==0 ): ?> <div class="spacer45"></div><hr /> <?php endif; ?>
                    
                    <h2 class="tcenter"><span><?php echo $i==0?$title:$amenity['title']; ?></span></h2>        
                    <div class="row clearfix">
                                    
                        <?php foreach ( $blocks as $block ): ?>
                             <div class='<?php echo $cssClass; ?>'>
                                <img src="<?php echo $block['image']['url']; ?>" />
                                <h4><?php echo $block['title']; ?></h4>
                                <div class="dst-italic"><?php echo $block['text']; ?></div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                <?php
                    endif;
                    $i++;
                endforeach;
            endif;
            
            $others = get_field( 'other_amenities', $relation[0] -> ID );
            if ( $others ):
            ?>
            <div class="row clearfix">
                <div class="col3">
                </div>
                <div class="col3" id="menu-limit">
                    <hr/>
                </div>
                <div class="col3">
                </div>
            </div>
            <div class="tcenter subtitle"><span><?php echo $others; ?></span></div>
            <div class="spacer45"></div>
            <?php
            endif;
            
            $statements = get_field( 'amenities_statements' , $relation[0] -> ID );
            if ( $statements ):
            ?>
            <div class="row clearfix">
            <?php
            foreach( $statements as $statement ):
                ?>
                <div class="col3">
                    <div class="indication">
                    <?php echo $statement['text']; ?>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
            </div>
            <?php
            endif;
            ?>
            
            
        </section>
    
    <?php endwhile; // end of the loop. ?>
    
    </div><!-- #main-wrapper -->

<?php get_footer(); ?>