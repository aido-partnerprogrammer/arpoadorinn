<?php
/**
 * Template Name: Temporada Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>


<div id="main" class="rooms-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar(); ?>
    </div>
	
    

<!--<div class="page-wide bgyellow">-->

	<div id="page-wrapper" class="margin-wide temporada-main-content">

        <div class="desktop">
            <h1 class="tcenter"><?php the_title() ?></h1>

            <div class="descricao">
                <p class="text">
                   <?php the_field( 'box_azul' ); ?>
                </p>
            </div>

            <div class="endereco">
                <?php the_field( 'box_preto' ); ?>
            </div>

            <div class="horario-funcionamento">
                <?php the_field( 'box_borda_salmao' ); ?>
            </div>
            <div class="christian">
                <img src="<?php the_field( 'imagem_chef' ); ?>">
            </div>
            <div class="carta-drinks">
                <?php the_field( 'box_salmao' ); ?>
            </div>
            <div class="descricao-christian">
                <?php the_field( 'box_borda_azul' ); ?>
            </div>
            <div class="temporada-comida">
                <img src="<?php the_field( 'imagem_01' ); ?>">
            </div>
            <div class="temporada-prato">
                <img src="<?php the_field( 'imagem_02' ); ?>">
            </div>
            <div class="temporada-view">
                <img src="<?php the_field( 'imagem_03' ); ?>">
            </div>
        </div>

        <div class="mobile">
            <h1 class="tcenter"><?php the_title() ?></h1>

            <div class="temporada-prato">
                <img src="<?php the_field( 'imagem_01_mobile' ); ?>" />
            </div>

            <div class="endereco">
               <?php the_field( 'box_preto' ); ?>
            </div>

            <div class="horario-funcionamento">
                <?php the_field( 'box_borda_salmao' ); ?>
            </div>

            <div class="descricao">
                <p class="text">
                    <?php the_field( 'box_azul' ); ?>
                </p>
            </div>
            <div class="temporada-view">
                <img src="<?php the_field( 'imagem_02_mobile' ); ?>">
            </div>
            <div class="temporada-comida">
                <img src="<?php the_field( 'imagem_03_mobile' ); ?>" />
            </div>
            <div class="christian">
                <img src="<?php the_field( 'imagem_04_mobile' ); ?>" />
            </div>
            <div class="descricao-christian">
                <?php the_field( 'box_borda_azul' ); ?>
            </div>
            <div class="temporada-prato">
                <img src="<?php the_field( 'imagem_05_mobile' ); ?>" />
            </div>
            <div class="temporada-taca">
                <img src="<?php the_field( 'imagem_06_mobile' ); ?>" />
            </div>
            <div class="carta-drinks">
                <?php the_field( 'box_salmao' ); ?>
            </div>
            <div class="temporada-comidas">
                <img src="<?php the_field( 'imagem_07_mobile' ); ?>" />
            </div>
        </div>
    
        <!-- RESTORANDO Widget Start -->
        <link href="<?php echo site_url(); ?>/restorando/restorando.css" rel="stylesheet" type="text/css" /> 
        <script src="http://mkt.restorando.com/widget/restorando.js" type="text/javascript"></script> 
        <div><a href="javascript:void(0);" onClick="parent.abrirFrame(320,490,'../restorando/reservas.html');" class="tab_reservas"><?php the_field( 'reservas', 'options' ); ?></a></div>
        <!-- RESTORANDO Widget End -->

    </div><!-- #main-wrapper -->
    
    
    <style type="text/css" media="screen">
        .temporada-main-content h1.tcenter {
            background: url(<?php the_field( 'logo_temporada' ); ?>) no-repeat center center;
        }
    </style>

<!--</div>-->

<?php get_footer(); ?>