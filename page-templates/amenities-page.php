<?php
/**
 * Template Name: Amenities Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>


<div id="main" class="amenities-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar(); ?>
    </div>

	<div id="page-wrapper" class="margin-wide">


<?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>


	<section>	
        <h1 class="tcenter"><?php the_title(); ?></h1>
   		<div class="tcenter subtitle"><?php echo the_excerpt(); ?></div>
		<div class="row clearfix">
        	<div class="col3">
            </div>
        	<div class="col3">
            	<hr/>
            </div>
        	<div class="col3">
            </div>
        </div>
                
        <?php
		$amenities = get_field( 'amenities' );
		if ( $amenities ) :
		foreach ( $amenities as $amenity ):
			$blocks = $amenity['amenities_block'];
			if ( $blocks ):
			?>
			<h2 class="tcenter"><span><?php echo $amenity['title']; ?></span></h2>
            <div class="row clearfix">
            <?php
                foreach ( $blocks as $block ):
                ?>
                <?php
                    $colLowercase = strtolower($amenity['title']);
                    $coltagCedilha = str_replace("ç", "c", "$colLowercase");
                    $coltagCedilhaM = str_replace("Ç", "c", "$coltagCedilha");
                    $coltagFinsh = str_replace(" ", "-", "$coltagCedilhaM");
                ?>
                <div class='<?php echo $coltagFinsh; ?>'>
                    <img src="<?php echo $block['image']['url']; ?>" />
                    <h4><?php echo $block['title']; ?></h4>
                    <div class="dst-italic"><?php echo $block['text']; ?></div>
                </div>
                <?php
				endforeach;
			?>
			</div>
            <?php
			endif;
		endforeach;
		endif;
		
		
		$others = get_field( 'other_amenities' );
		if ( $others ):
		?>
		<div class="row clearfix">
        	<div class="col3">
            </div>
        	<div class="col3">
            	<hr/>
            </div>
        	<div class="col3">
            </div>
        </div>
   		<div class="tcenter subtitle"><?php echo $others; ?></div>
        <div class="spacer45"></div>
        <?php
		endif;
		
		$statements = get_field( 'amenities_statements' );
		if ( $statements ):
		?>
        <div class="row clearfix">
        <?php
		foreach( $statements as $statement ):
			?>
            <div class="col3">
            	<div class="indication">
            	<?php echo $statement['text']; ?>
                </div>
            </div>
            <?php
		endforeach;
		?>
        </div>
        <?php
		endif;
        ?>
                
    </section>

<?php endwhile; // end of the loop. ?>
    </div><!-- #main-wrapper -->

<?php get_footer(); ?>