<?php
/**
 * Template Name: Sustentabilidade
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>

<div id="main" class="sustentabilidade-page">

  <!-- booking bar -->
  <div id="book-now-bar" class="page-wide"><?php get_bar(); ?></div>

	<div id="page-wrapper" class="margin-wide">  
    <?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>
      <section>	
        <h1 class="tcenter sr-only"><?php the_title(); ?></h1>

        <div class="clearfix">
          
          <div class="section section-chamada">
            
            <?php if (get_field('chamada_titulo')) :?>
              <h2><?php the_field('chamada_titulo'); ?></h2>
            <?php endif; ?>
            
            <?php if (get_field('chamada_subtitulo')) :?>
              <div class="subtitulo">
                <?php the_field('chamada_subtitulo'); ?>
              </div>
            <?php endif; ?>
            
            <?php if (get_field('chamada_imagem')) :?>              
              <img src="<?php the_field('chamada_imagem'); ?>" class="img-fluid mt-md-4">
            <?php endif; ?>

          </div> <!-- section-chamada -->


          <div class="section section-depoimento">
            
            <?php if (get_field('depoimento_titulo')) :?>
              <h2><?php the_field('depoimento_titulo'); ?></h2>
            <?php endif; ?>
            
            <?php if (get_field('depoimento_subtitulo')) :?>
              <div class="subtitulo">
                <?php the_field('depoimento_subtitulo'); ?>
              </div>
            <?php endif; ?>
            

            <div class="row my-md-5">

              <div class="col-md-4">
                <?php if (get_field('depoimento_imagem')) :?>              
                  <img src="<?php the_field('depoimento_imagem'); ?>" class="img-fluid mt-5 mb-4 my-md-0">
                <?php endif; ?>
              </div>

              <div class="col-md-8 col-lg-7 d-md-flex align-items-md-center ">
                <?php if (get_field('depoimento_texto')) :?>              
                  <div class="depoimento mb-md-0">
                    <?php the_field('depoimento_texto'); ?>
                  </div>
                <?php endif; ?>
              </div>
              
            </div>            

          </div> <!-- section-depoimento -->
          <hr />

          <div class="section section-artigos">
            
            <?php if (get_field('artigos_titulo')) :?>
              <h2><?php the_field('artigos_titulo'); ?></h2>
            <?php endif; ?>
            
            <?php if (get_field('artigos_descricao')) :?>
              <div class="subtitulo">
                <?php the_field('artigos_descricao'); ?>
              </div>
            <?php endif; ?>
            
            <div class="row pilares mt-md-5">
              <?php 
                $pilares = get_field('artigos');
                
                if ($pilares) {
                  foreach ($pilares as $pilar ) {
              ?>
                  <div class="col-md-4 col-lg-3 mt-3">
                      <?php if (isset($pilar['imagem'])) : ?>
                        <img src="<?php echo $pilar['imagem']; ?>" class="img-fluid mb-1" />
                      <?php endif ?>
                      <p class="mb-0"><strong><?php echo $pilar['titulo']; ?></strong></p>
                      <p class="desc"><?php echo $pilar['descricao']; ?></p>
                  </div>
              <?php
                  }
                }
              ?>
            </div>            

          </div> <!-- section-artigos -->
          
          <hr />

          
          <div class="section section-apoio">
                       
            <div class="row">

              <div class="col-md-4">
                <?php if (get_field('apoio_imagem')) :?>              
                  <img src="<?php the_field('apoio_imagem'); ?>" class="img-fluid mb-3">
                <?php endif; ?>
              </div>

              <div class="col-md-8 col-lg-6">
                <?php if (get_field('apoio_titulo')) :?>
                  <h2><?php the_field('apoio_titulo'); ?></h2>
                  <br />
                <?php endif; ?>
                <?php if (get_field('apoio_descricao')) :?>              
                  <div class="descricao">
                    <?php the_field('apoio_descricao'); ?>
                  </div>
                <?php endif; ?>
              </div>
              
            </div>

            

          </div> <!-- section-apoio -->


        </div> <!-- clearfix --> 
      </section>
    <?php endwhile; // end of the loop. ?>
  </div><!-- #page-wrapper -->


<?php get_footer(); ?>