<?php
/**
 * Template Name: ARP Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>

<div id="main" class="arp-page">

  <!-- booking bar -->
  <div id="book-now-bar" class="page-wide"><?php get_bar(); ?></div>

	<div id="page-wrapper" class="margin-wide">
    
  <?php while ( have_posts() ) : the_post(); $id = get_the_ID(); ?>
    <section>	
      <h1 class="tcenter sr-only"><?php the_title(); ?></h1>

      <div class="row clearfix ">
        <div class="arp-header">
          <div class="arp-header__mobile d-md-none">
            <?php if ( get_field('arp_thumb') ) : ?>
              <img src="<?php the_field('arp_thumb'); ?>" alt="<?php the_title(); ?>" class="mb-3">
            <?php endif; ?>
            
            <div class="media">
              <img src="<?php the_field('arp_logo'); ?>" alt="<?php the_title(); ?>" class="arp-logo mr-5">
              <div class="media-body">
                <?php if ( get_field('arp_content_header') ) : ?>
                  <?php echo get_field('arp_content_header'); ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <div class="arp-header__left d-none d-md-block">
            <img src="<?php the_field('arp_logo'); ?>" alt="<?php the_title(); ?>" class="arp-logo">
            <?php if ( get_field('arp_content') ) : ?>
              <?php echo get_field('arp_content'); ?>
            <?php endif; ?>
          </div>
          <div class="arp-header__right">
            <div id="arp-grid">
              <?php
                if( have_rows('mosaic') ):
                  $i=1; while ( have_rows('mosaic') ) : the_row();
                    $hide = ($i==1 && get_row_layout() == 'flexible_full_image') ? 'hide-mobile' : '';
                  
                    if( get_row_layout() == 'flexible_full_image' ):
                      echo '<div class="item item--width-100 '.$hide.'" style="background-image: url('.get_sub_field('image').');"></div>';
                          
              
                      elseif( get_row_layout() == 'flexible_content_image' ):
                        $align   = get_sub_field('align');
                        $bg      = get_sub_field('bg');
                        $border  = get_sub_field('border');
                        $text    = get_sub_field('text');
                        $content = get_sub_field('content');

                        $box = ($content) ? 'auto-height' : '';

                        if ($align == 'ltr') {
                          echo '<div class="wrap-content block-'.$i.' ltr">',
                                  '<div class="item item--width-4 item--height-2 height-auto" style="background-image: url('.get_sub_field('image').');"></div>',
                                  '<div class="item item--width-8 item--height-2 height-auto '.$box.'" style="background-color: '.$bg.'; color: '.$text.'; border: 1px solid '.$border.';">',
                                    '<div class="content">'.$content.'</div>',
                                  '</div>',
                                '</div>';
                        } else {
                          echo '<div class="wrap-content block-'.$i.' flex-row-reverse">',
                                  '<div class="item item--width-4 item--height-2 height-auto" style="background-image: url('.get_sub_field('image').');"></div>',
                                  '<div class="item item--width-8 item--height-2 height-auto '.$box.'" style="background-color: '.$bg.'; color: '.$text.'; border: 1px solid '.$border.';">',
                                    '<div class="content">'.$content.'</div>',
                                  '</div>',
                                '</div>';
                        }
                      elseif( get_row_layout() == 'flexible_content_images' ):
                        $align   = get_sub_field('align');
                        $height  = get_sub_field('height');
                        $bg      = get_sub_field('bg');
                        $border  = get_sub_field('border');
                        $text    = get_sub_field('text');
                        $content = get_sub_field('content');

                        $box = ($content) ? 'auto-height' : '';

                          echo '<div class="wrap-content block-'.$i.'">',
                                  '<div class="item item--width-2 item--height-3" style="background-image: url('.get_sub_field('image').');"></div>',
                                  '<div class="item item--width-2 item--height-3" style="background-image: url('.get_sub_field('image_sec').');"></div>',
                                  '<div class="item item--width-4 '.$box.'" style="background-color: '.$bg.'; color: '.$text.'; border: 1px solid '.$border.';">',
                                    '<div class="content">'.$content.'</div>',
                                  '</div>',
                                '</div>';

                      elseif( get_row_layout() == 'flexible_content_images_sec' ):
                        $height = (get_sub_field('height')) ? 'item--height-7' : '';

                          echo '<div class="wrap-content block-'.$i.'">',
                                  '<div class="item item--width-8 '.$height.'" style="background-image: url('.get_sub_field('image').');"></div>',
                                  '<div class="item item--width-4 '.$height.' item--height-4" style="background-image: url('.get_sub_field('image_sec').');"></div>',
                                '</div>';
                                
                      elseif( get_row_layout() == 'flexible_content_three_images' ):
                          echo '<div class="wrap-content block-'.$i.'">',
                                  '<div class="item item--width-2 item--height-5" style="background-image: url('.get_sub_field('image').');"></div>',
                                  '<div class="item item--width-2 item--height-5" style="background-image: url('.get_sub_field('image_sec').');"></div>',
                                  '<div class="item item--width-4 item--height-4" style="background-image: url('.get_sub_field('image_ter').');"></div>',
                                  '<div class="item item--width-8 item--height-7" style="background-image: url('.get_sub_field('image_qua').');"></div>',
                                '</div>';

                      endif;
              
                  $i++; endwhile;
                endif;
              ?>
            </div>
          </div>

          <div class="arp__header__mobile d-md-none">
            <?php if ( get_field('arp_content_footer') ) : ?>
              <?php echo get_field('arp_content_footer'); ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; // end of the loop. ?>
</div><!-- #main-wrapper -->

<?php get_footer(); ?>