/*
 *  Project: slideshow
 *  Description:
 *  Author: filippo della casa
 *  License:
 
 dependences: jquery.easing.1.3.js, jquery.imagesloaded.min.js, jquery.transit.min.js
 
 */

// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ($, window, document, undefined) {

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "slideshow_wildcard";
    var defaults = {
        autoplay: true,
		time: 4000,
		time_transition: 777,
		mode: 'fade',
		transition_on: 'easeOutCirc',
		transition_off: 'easeInCirc',
		pauseover: true
    };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
		
		this.index_slideshow = 0;
		this.loaded = [];
		this.ordims = [];
		this.timeout_slideshow;
		this.transition_active = false;
		this.max_slides;
		this.over = false;
		
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.options
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.options).
			this.max_slides = $(this.element).find('.slide-wildcard').length;
			
			//console.log( this.max_slide )
			if ( $(this.element).find('.slide-wildcard').length <= 1 ){
				this.options.autoplay = false;
				$(this.element).find('.navigation-wildcard').css('display','none');
			}
			var _this = this;
						
			$(this.element).find('.slide-wildcard').each( function( i, e ){
				var $slide = $(this);
				var $img = $slide.find('img');
				_this.ordims[i] = [ $img.attr( 'width' ), $img.attr( 'height' ) ];
				$slide.imagesLoaded( function( $images, $proper, $broken ) {
					_this.loaded[i] = $images.length >= 1 ? true : false;
					$images.each( function(i,e){
						$(this).attr( 'width', $(this).width() );
						$(this).attr( 'height', $(this).height() );
					});
					if ( _this.loaded[0] ){
						/*
						$slide.css( {'background-image': 'url(' + $img.attr('src') + ')' } );
						$img.css('display', 'none' );
						*/
						//console.log( $slide.css('background') + ' IMG::::  ' + $img.attr('src')  ) 
						_this.revealImage( _this.index_slideshow, true );
					}
				}); 
			});
			
			// nav
			$(this.element).find('.navigation-wildcard li').each( function(i,e){
				$(this).children('div').click( function(e){
					if ( i != _this.index_slideshow && _this.loaded[_this.index_slideshow] ){
						_this.nextImage( i );
					}
				} );
			} );
			
			// pause on mouse hover
			if ( this.options.pauseover && this.options.autoplay ){
				$(this.element).hover( function(e){
					_this.over = true;
					if ( _this.options.autoplay ) clearTimeout( _this.timeout_slideshow );
				}, function(e){
					_this.over = false;
					if ( _this.options.autoplay ) _this.timeout_slideshow = setTimeout( function(){  _this.nextImage(); }, _this.options.time );
				} );
			};
			
			/* SWIPE ! */
			$(this.element).swiperight( function(e, touch){
				var i = _this.index_slideshow - 1;
				if ( i< 0 ) i = _this.max_slides - 1;
				_this.slideImage( i , 'swiperight' );
			} );
			$(this.element).swipeleft( function(e, touch){
				var i = _this.index_slideshow + 1;
				if ( i>= _this.max_slides ) i = 0;
				_this.slideImage( i, 'swipeleft' );
			} );

        },
		revealImage: function( i, b ){
			var _this = this;
			var $slide = $(this.element).find('.slide-wildcard:eq('+ i +')');
			if ( this.options.mode == 'fade' ){
				if ( b ){
					$slide.css( 'z-index' , 0 );
					$slide.transition( {opacity:b?1:0}, this.options.time_transition, b ? this.options.transition_on : this.options.transition_off , function(){
						_this.onEndReveal();						
					});
				}
				else{
					$slide.css( 'z-index' , -1 );
				}
			}
		},
		onEndReveal: function(){
			this.transition_active = false;
			clearTimeout( this.timeout_slideshow );
			var _this = this;
			$(this.element).find('.slide-wildcard').each( function (i, e){
				if ( i != _this.index_slideshow ) {
					$(this).css('opacity',0);
					//console.log( '$(this) = ' + $(this) + ', i = ' + i )
				}
			} );
			if ( this.options.autoplay && !this.over ) this.timeout_slideshow = setTimeout( function(){  _this.nextImage(); }, this.options.time );
		},
		nextImage: function( next ){
			if ( this.transition_active ) return;
			this.transition_active = true;
			var n = next ? next : this.index_slideshow + 1;
			if ( n >= this.max_slides ) n = 0;
			if ( this.loaded[n] ){
				this.revealImage( n, true );
				this.revealImage( this.index_slideshow, false );
				this.updateNavPoints(this.index_slideshow,n);
				this.index_slideshow = n;
			}
			else{
				onEndReveal()	
			}
		},
		slideImage: function( n, swipe ){
			var _this = this;
			if ( this.transition_active ) return;
			if ( this.max_slides <= 1 ) return;
			if ( this.loaded[n] ){
				this.transition_active = true;
				clearTimeout( this.timeout_slideshow );
				var $cslide =  $(this.element).find('.slide-wildcard:eq('+ this.index_slideshow +')');
				var $nslide =  $(this.element).find('.slide-wildcard:eq('+ n +')');
				var w = $cslide.width();
				$nslide.css('left', swipe == 'swipeleft' ? w : -w );
				$nslide.css('opacity',1);
				$nslide.transition( {left:0}, this.options.time_transition, this.options.transition_off );
				$cslide.transition( {left: swipe == 'swipeleft' ?  -w : w }, this.options.time_transition, this.options.transition_off , function(){ 
					$(this).css('opacity',0);
					$(this).css('left',0);
					_this.onEndReveal()
				});
				this.updateNavPoints( this.index_slideshow, n );
				this.index_slideshow = n;
			}
		},
		updateNavPoints: function( i, n ){
			$(this.element).find('.navigation-wildcard li:eq('+ i +')').children('div').removeClass('point-selected');
			$(this.element).find('.navigation-wildcard li:eq('+ n +')').children('div').addClass('point-selected');
		}
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);