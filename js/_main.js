
var ismobileLayout = false;

var offset,$htmlBody,$window;
var barFixedInPages = true;
var device;

$(document).ready(function(e) {
	$htmlBody = $('html,body');
	offset  = $('header').height();
	$window   = $(window);	
	
	if ( $('body').hasClass( 'home' ) ) {
		initHome();
	}
	else{
	}

	
	if ( $('#main').hasClass('rooms-page') ) initRooms();
	if ( $('#main').hasClass('cityandsea-page') ) initCityAndSea();
	if ( $('#main').hasClass('guests-page') ) initGuests();
	if ( $('#main').hasClass('contact-page') || $('body').hasClass( 'home' ) ) initContact();

	//initBookNowBar();
	
	// city and the sea fix menu selection when category and single
	if ( $('body').hasClass( 'category' ) || $('body').hasClass( 'single' ) ){
		// pt
		$( '#menu-item-114' ).addClass( 'current_page_item' );
		// en
		$( '#menu-item-123' ).addClass( 'current_page_item' );
	} 
				
	// RESIZE
  	$window.resize( function(e){
		onResize(e)
	});
	onResize();
	
	if ( ismobileLayout ){
		$('#mobile-wrap-container').scroll( onUserScroll );
	}
	else{
		$window.scroll(  onUserScroll );
	}

	initMobileMenu();
	
	//initMaps()
});

$(window).load(function(e){
	// date picker
	initBookNowBar();
});


function initMobileMenu(){
	$('#mobile-icon').click(function(e){
		toggleMobileMenu()
	});
	$('#drawer').data('open', false);
}
function toggleMobileMenu(){
	var toOpen = !$('#drawer').data('open');
	$('#drawer').animate({left:toOpen?0:-200}, 477, 'easeInOutQuad' );
	$('#mobile-wrap').animate({left:toOpen?200:0}, 377, 'easeInOutQuad' );
	$('#drawer').data('open', toOpen);
}



function onResize(e){
	
	switch ( $('#mediaquery').css('float') ){
		case 'left':
			device = 'ipad';
		break;
		case 'right':
			device = 'iphone';
		break;
		default:
			device = 'desktop';
	}
	//console.log( 'device = ' + device + ', float = ' + $('#mediaquery').css('float') );
	var w = $window.width();
	var h = $window.height();
	var ww = $('#header-container').width();
	var margin = parseInt( $('.margin-wide').css('margin-left') );
	

	// mobile!
	if ( ismobileLayout ){
		$('#mobile-wrap').width(w);
		$('#mobile-wrap').height(h);
	}




	/* BOOK NOW BAR FIX!!!!! */
	if ( $('body').hasClass('home') ){
		$('#book-now-bar').width(w-margin*2);
	}
	else{
		margin = parseInt( $('.page-wide').css('margin-left') );
		$('#book-now-bar').width(w-margin*2);
		if ( barFixedInPages ){
			$( '#book-now-bar' ).addClass( 'bar-locked-page' );
			$( '#page-wrapper' ).css( 'margin-top', $( '#book-now-bar' ).height() + $('#masthead').height() );	
		}
	}
	resizeGrid();
	layoutInstagram();
	if ( $('#main').hasClass('guests-page') ) resizeGuests();
	if ( $('body').hasClass('home') || $('#main').hasClass('contact-page')  ) resizeMap();
}

var userclick = false;
function scrollMenu( id ){
	var time = 577;
	userclick = true;
	var toy = id == '#home' ? 0 : $(id).offset().top-offset;
	$htmlBody.animate({scrollTop: toy }, time, 'easeInOutCirc', function(){
		setTimeout( function(){
			userclick = false;
		}, 200 );
	} );  
}




function initHome(){
	//initSlideShow();
	initSlideShow( 4000, undefined, [1500,655] );
	initHomeRooms();
	loadHomeInstagram();
}

function initRooms(){
	initSlideShow();
	initLayoutRooms()
}

var fromDatePicker, toDatePicker;
function initBookNowBar(){
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	
	var fromDate = now;
	// next day
	var toDate = new Date( now ); 
	toDate.setDate( toDate.getDate() + 1 );
	
	
	
	var dowNames = $('html').attr('lang') == 'en-US' ? [ 'S', 'M', 'T', 'W', 'T', 'F', 'S' ] : [ 'D', 'S-F', 'T-F', 'Q-F', 'Q-F', 'S-F', 'S' ];
	var monthNames = $('html').attr('lang') == 'en-US' ? [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ] : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ];
	
	
	
	
	fromDatePicker = $('#from').glDatePicker( {
		cssName: 'flatwhite'
		,dowNames: dowNames
		,monthNames: monthNames
		,width:260
		,prevArrow:'←'
		,nextArrow:'→'
		, onClick: (function(el, cell, date, data) {
			if ( date.getTime() < now.getTime() ){
				fromDatePicker.options.selectedDate = new Date(); //fromDate;
				fromDatePicker.render();
				el.val( el.attr('data-default') );
			}
			else{
        		el.val(date.toLocaleDateString());
				fromDate = date;
				if ( toDatePicker.options.selectedDate.getTime() == now.getTime() && toDatePicker.options.selectedDate.getTime() <= date.getTime() ){
					toDate = new Date( date ); 
					toDate.setDate( toDate.getDate() + 1);
					toDatePicker.options.selectedDate = toDate;
					toDatePicker.render();
					$('#to').val( toDate.toLocaleDateString() );
				}
				toDatePicker.show()	
			}
    	})
		, onShow: function(calendar) { 
			setTopPosition( calendar, fromDatePicker )
		}

	}).glDatePicker(true);
	
	toDatePicker = $('#to').glDatePicker({
		cssName: 'flatwhite'
		,dowNames: dowNames
		,monthNames: monthNames
		,width:260
		,prevArrow:'←'
		,nextArrow:'→'
		, onClick: (function(el, cell, date, data) {
			if ( date.getTime() <= fromDate.getTime() ){
				// set disable!	
				toDatePicker.options.selectedDate = new Date();//toDate;
				toDatePicker.render();
				el.val( el.attr('data-default') );
			}
			else{
        		el.val(date.toLocaleDateString());
				toDate = date;
			}
    	})
		, onShow: function(calendar) { 
			setTopPosition( calendar, toDatePicker );
		}
	}).glDatePicker(true);
	
	function setTopPosition( calendar, which ){
		if ( $( '#book-now-bar').hasClass('bar-locked') ){
			calendar.css( {top: ( $(window).scrollTop() + $( '#book-now-bar').position().top + 43 + which.options.calendarOffset.y ) + 'px'} ); 
		}
		else{
			var el = $('#from');
			var elPos = el.offset();
			calendar.css(
			{
				top: (elPos.top + el.outerHeight() + which.options.calendarOffset.y ) + 'px',
				//left: (elPos.left + options.calendarOffset.x) + 'px'
			});
		}
		calendar.show(); 
	};
	
	
	// select
	$('#adult').change(function(e) {
        $('#uniform-adult span').html(e.currentTarget.value)
    });

    initBooking()
}

function initBooking(){
	// mobile
	$('.book-now-mobile').click( function(e){
		//e.preventDefault();
		window.open( getiniUrl() );
	});
	// desktop
	$('a#book-now-bar-main').click( function(e){
		e.preventDefault();
		var iniUrl = getiniUrl();
		var from = $('#from').val();
		var to = $('#to').val();
		if ( from !== 'check-in' && to !== 'check-out' ){
			iniUrl += '&arrive=' + formatdate(from);
			iniUrl += '&depart=' + formatdate(to);

			iniUrl += '&start=availresults';			
		}
		if ( $('#adult').val() ){
			iniUrl += '&adult=' + $('#adult').val();
		}
		window.open( iniUrl );
	})
}
function getiniUrl(){
	var $btn = $('#book-now-btn');
	var url = $btn.attr('data-url');
	var hotel = $btn.attr('data-hotel');
	var chain = $btn.attr('data-chain');
	var locale = $btn.attr('data-locale');
	var iniUrl = url + '?Hotel=' + hotel + '&Chain=' + chain + '&locale=' + locale;
	return iniUrl	
}
function formatdate(s){
	var locale = $('#book-now-btn').attr('data-locale');	
	var a = s.split('/');
	var dd, mm, yyyy;
	var str = '';
	for ( var i=0; i<a.length; i++){
		var v = a[i];
		if ( i < 2 ){
			if ( v.length === 1 ){
				v = '0' + v;
			}
			if ( i === 0 ){
				mm = v;
			}
			else{
				dd = v;
			}
		}
		else{
			yyyy = v
		}
	}
	return encodeURIComponent( locale === 'en-US' ? ( mm + '/' + dd + '/' + yyyy ) : ( dd + '/' + mm + '/' + yyyy ) );
}


function initCityAndSea(){
	var tot = $('#slide-txt ul li').length;
	initSlideShow( 8000 , function(n){
		$('#slide-txt ul').transition( {left: n == tot ? 0 : -n*($('#slide-txt ul li').width() + parseInt($('#slide-txt ul li').css('margin-right'))) }, 700, 'easeInOutCirc'  );	
	} );
	// categories
	// link to all categories
	$('ul#categories li:eq(0) a').attr('href',  $('ul#categories li:eq(0) a').attr('href') + 'city-sea' );
	if ( $('#categories li.current-cat').length == 0 ){
		$('ul#categories li:eq(0)').addClass( 'current-cat' );
	}
	
	// pagination
	$('a.pagination-links').click(function(e) {
	   	e.preventDefault();
		$(this).parents('form').submit();	
	});
	// grid
	if ( $('#grid-posts').length > 0 ){
		
		
		$('#tiles').css('position', 'relative');
			
      $('#tiles').imagesLoaded(function() {
        // Prepare layout options.
		resizeGrid();
      });
	}
}

function resizeGrid(){
	// Get a reference to your grid items.
	var handler = $('#tiles li');

	if ( handler.length == 0 ) return;
	
	var offset = $(window).width() > 480 ? 50 : 0;
	var w = $(window).width() > 480 ? 200 : 300;
	
	var options = {
	  autoResize: false, // This will auto-update the layout when the browser window is resized.
	  container: $('#grid-posts'),// Optional, used for some extra CSS styling
	  offset: offset, // Optional, the distance between grid items
	  itemWidth: w // Optional, the width of a grid item
	  
	  ,align: 'left'
	};
	// Call the layout function.
	handler.wookmark(options);

	setTimeout( function(){
		handler.wookmark();
	}, 77 );

}

// USER SCROLL
function onUserScroll(){  
	var top =  ismobileLayout  ? $('#mobile-wrap-container').scrollTop() : $window.scrollTop();

	//console.log( top )

	var offset = parseInt ( $('#main').css('margin-top') );
	
	
	if ( device != 'desktop' ) return;
	
	if ( $('header').css( 'position' ) == 'fixed' ){ 
	
		if ( $('body').hasClass('home') ){	
			if ( top > $('#home-header').height()- $('header').height() ){ // slideshow height + difference between headers
				if ( !$( '#book-now-bar').hasClass('bar-locked') ){
					
					$("#slides").css( 'margin-top', $( '#book-now-bar' ).height() ); 
					$( '#book-now-bar' ).addClass( 'bar-locked' );
					
					$('#logo-small').stop().transition({opacity:1}, 1377, 'easeInQuad' );
					$('#logo-small').css('display','block');
					$('#lang-switch').stop().transition({opacity:1}, 1377, 'easeInQuad' );
					$('#lang-switch').css('display','block');
					
					
					//onResize()
					
					//console.log( $( '#book-now-bar').width() )
				}
			}
			else{
				if ( $( '#book-now-bar').hasClass('bar-locked') ){
					
					$( '#book-now-bar' ).removeClass( 'bar-locked' );
					$("#slides").css( 'margin-top', 0 ); 
					
					$('#logo-small').stop().transition({opacity:0}, 277 ,'easeInQuad' , function(){
						$(this).css('display','none');
					});
					$('#lang-switch').stop().transition({opacity:0}, 277 ,'easeInQuad' , function(){
						$(this).css('display','none');
					});
					
					//console.log( $( '#book-now-bar').width() )
				}
			}
		}
		else{
			if ( top > $('#book-now-bar').height() ){
				if ( !barFixedInPages ){				
					if ( $('#line-bottom').hasClass('invisible') ){
						$('#line-bottom').removeClass('invisible')
						$('#book-now-btn').stop().transition({opacity:1}, 1377, 'easeInQuad' );
						$('#book-now-btn').css('display','block');
					}
				}
			}
			else{
				if ( !barFixedInPages ){
					if ( !$('#line-bottom').hasClass('invisible') ){
						$('#line-bottom').addClass('invisible')
						$('#book-now-btn').stop().transition({opacity:0}, 277, 'easeInQuad', function(){
							$(this).css('display','none');
						} );
					}
				}
			}
		}
	
		if ( fromDatePicker ) fromDatePicker.hide();
		if ( toDatePicker ) toDatePicker.hide();
		
		// menu pages
		if ( $('#main').hasClass('rooms-page') ){
	
			if ( barFixedInPages ) offset += $('#book-now-bar').height();
			
			if ( $('#nav-rooms-header').css( 'position') != 'fixed' ){
				if ( top > Math.ceil( $('#nav-rooms-header').offset().top - offset ) + 1 ){
					$('#nav-rooms-header').css( 'position', 'fixed' );
					$('#nav-rooms-header').css( 'top', offset -1 );
					$('#slideshow').css( 'margin-top', $('#nav-rooms-header').height() );
				}
			}
			else{
				if ( top < $('#slideshow').offset().top - $('#nav-rooms-header').height() - offset ){
					$('#nav-rooms-header').css( 'position', 'relative' );
					$('#nav-rooms-header').css( 'top', 'auto' );
					$('#slideshow').css( 'margin-top', 0 );
				} 
			}
		}
	}
}

function initSlideShow( speed, callback, sizes ){
	console.log( sizes );

	var w = 996;
	var h = 498;
	if ( sizes !== undefined ){
		w = sizes[0];
		h = sizes[1];
	}	
	var autoplay = $(".slides img").length > 1;
	$(".slides").slidesjs( {
		width: w,
		height: h,
		callback: {
		  loaded: function(number) {
			// Use your browser console to view log
			//console.log('SlidesJS: Loaded with slide #' + number);
		  },
		  start: function(number) {
			// Use your browser console to view log
			//console.log('SlidesJS: Start Animation on slide #' + number);
			if ( callback ) callback(number)
		  },
		  complete: function(number) {
			// Use your browser console to view log
			//console.log('SlidesJS: Animation Complete. Current slide is #' + number);
		  }
		}
		,navigation: false
		,pagination: {
			 effect: "fade"
		}
		,play: {
		  active: false,
			// [boolean] Generate the play and stop buttons.
			// You cannot use your own buttons. Sorry.
		  effect: "fade",
			// [string] Can be either "slide" or "fade".
		  interval: speed || 4000,
			// [number] Time spent on each slide in milliseconds.
		  auto: autoplay,
			// [boolean] Start playing the slideshow on load.
		  swap: false,
			// [boolean] show/hide stop and play buttons
		  pauseOnHover: true,
			// [boolean] pause a playing slideshow on hover
		  restartDelay: 500
			// [number] restart delay on inactive slideshow
		}
		,effect: {
			  slide: {
				// Slide effect settings.
				speed: 200
				  // [number] Speed in milliseconds of the slide animation.
			  },
			  fade: {
				speed: 700,
				  // [number] Speed in milliseconds of the fade animation.
				crossfade: true
				  // [boolean] Cross-fade the transition.
			  }
		}
	});

	if ( !autoplay ){
		$(".slides a").addClass('nop');
	}

}

function initLayoutRooms(){
	if ( $('#layout-images img').length > 1 ){
		$('#layout-images').imagesLoaded( function( $imgs ){
			var h = 0; 
			var maxi = 0;
			$imgs.each( function ( i, el ){
				if ( i > 0 ) {
					$(this).css( 'top', - h );
					$(this).css( 'opacity', 0 );
				}
				h += $(this)[0].height;
				if ( $(this)[0].height > maxi ) maxi = $(this)[0].height;
			} );
			$('#layout-images').height(maxi);
			
			$('#layout li').each( function(i, el){
				$(this).click( function(e){
					setLayout( i );
				} );
				if ( i == 0 ) $(this).addClass('active');
			} );
			$(this).show();
		});
		function setLayout( i ){
			$('#layout li').each( function(j, el){
				if ( j == i ) {
					$(this).addClass('active');
					$('#layout-images img:eq('+ j +')').transition({ opacity:1}, 400, 'easeOutQuad' );
				}
				else{
					$(this).removeClass('active');
					$('#layout-images img:eq('+ j +')').transition({ opacity:0}, 200, 'easeInQuad' );
				}
			});
		}
	}
	else
		$('#layout-images').show();
}



function initGuests(){
	
	
	/* user photos */
	var tot = $('#slide-txt ul li').length;
	initSlideShow( 8000 , function(n){
		//$('#slide-txt ul').transition( {left: n == tot ? 0 : -n*($('#slide-txt ul li').width() + parseInt($('#slide-txt ul li').css('margin-right'))) }, 700, 'easeInOutCirc'  );	
		$('#slide-txt ul').transition( {opacity: 0 }, 200, 'easeOutCirc', function(){
			$(this).css('left',n == tot ? 0 : -n*($('#slide-txt ul li').outerWidth() + parseInt($('#slide-txt ul li').css('margin-right'))) );
			$(this).transition( {opacity: 1 }, 300, 'easeOutCirc' );
		}  );	
	} );
	
	
	
	/* instagram */
	var loading = true;
	var insta_container = $("#instagram"), insta_next_url;
	/* init */
	insta_container.instagram({
	  hash: 'ipanema'
	, userId: 4907783  /* ferdydurque from http://jelled.com/instagram/lookup-user-id */
	, clientId : '9023c8ad76d44f2398a9da6bc0989299'
	, show : 18
	, append: false
	, image_size: 'standard_resolution'
	, onComplete : function (photos, data) {
			insta_next_url = data.pagination.next_url;
			appendPhotos( data.data );
			loading = false;
		}
	});
	function loadNextInstagram(){	
		if ( !loading ){
		  loading = true;
		  insta_container.instagram({
			  next_url : insta_next_url
			, show : 11
			, append: false
			, onComplete : function(photos, data) {
			  insta_next_url = data.pagination.next_url;
			  appendPhotos( data.data );
			  if ( insta_next_url ){
				   loading = false;
			  }
			  else{
			    $('a.instamore').hide(0);  
			  }
			  
			}
		  })
		}       
	}; 
	/* append photos */
	function appendPhotos( arr ){
		console.log( arr )
		var stream = '';
		for ( var i=0; i<arr.length; i++ ){
			var o = arr[i];
			var lnk = o.link;
			var txt = o.caption ? o.caption.text : '';
			stream += '<div class="insta">';
				stream += '<a href="'+ lnk +'" target="_blank"><img src="' + o.images.standard_resolution.url + '" class="pt"/></a>';
				//stream += '<div><img src="' + o.user.profile_picture + '" class="pu"/><span class="sb">' + o.user.username + '</span><br/>' + txt + '</div>';
				//stream += '<center><a href="'+ lnk +'" target="_blank" class="like">' + o.likes.count + ' Likes</a></center>';
				stream += '<div><span class="sb">— <a href="http://instagram.com/'+ o.user.username +'" target="_blank">' + o.user.username + '</a></span></div>';
			stream += '</div>';
		}	
		
		//console.log( stream )
		insta_container.append( stream );
		
		insta_container.imagesLoaded(function() {
			layoutInstagram()
		});
	};
	$('a.instamore').click(function(e) {
        e.preventDefault();
		loadNextInstagram();
    });
	
	
	// tweets
	twitterFetcher.fetch('362653893966053376', '', 5, true, true, true, '', false, handleTweets);
	  function dateFormatter(date) {
		return date.toTimeString();
	  }

	function handleTweets(tweets){
	console.log( tweets )
	  var x = tweets.length;
	  var n = 0;
	  var html = '<ul>';
	  while(n < x) {
		html += '<li>' + tweets[n] + '</li>';
		n++;
	  }
	  html += '</ul>';
	  $('#tweets').append( html );
	  $('#tweets').removeClass( 'loading' );
	  
	}
}

function resizeGuests(){
	
}


function initHomeRooms(){
	$('.grid-rooms').imagesLoaded( function(){
		// set scrolls
		$('.grid-slider').each(function(index, element) {
            if ( $(this).hasClass( 'orizontal' ) ){
				$(this).width( 4*$(this).parents('.room-img-grid').width() );
			};
			// set current slide
			var n = parseInt($(this).attr('rel'));
			setCurrentSlide( n , index );
        });
		$('.room-img-grid div.over').each(function(index, element) {
			var $this = $(this);
            $this.css({ 'line-height': $(this).height() + 'px'});
			$this.hover( 
				function(e){
					$this.stop().transition({ opacity:1}, 200, 'easeOutQuad' );
					setCurrentSlide( parseInt($(this).attr('rel')) , -1 );
				}, 
				function(e){
					$this.stop().transition({ opacity:0}, 400, 'easeInQuad' );	
					setCurrentSlide( -1 , -1 )				
				} 
			);
        });
		
		function setCurrentSlide( n, index ){
			//console.log( '>> setCurrentSlide n = ' + n + ', index = ' + index  );
			var $el = $('.grid-slider');
			if ( index != -1 ){
				$el = $('.grid-slider:eq(' + index + ')' );
			}
			var qual = n;
			$el.each( function( i, element ){
				if ( n == -1 ){
					qual = parseInt($(this).attr('rel'));
				}
				if ( $(this).hasClass( 'orizontal' ) ){
					$(this).stop().transition({ left: - qual*$(this).parents('.room-img-grid').width() }, 270 + i*37, 'easeInOutCirc' );
				}
				else{
					$(this).stop().transition({ top: - qual*$(this).parents('.room-img-grid').height() }, 270 + i*37, 'easeInOutCirc' );
				}
			});
		};

	});
};



function layoutInstagram(){
	if ( ! $('#main').hasClass('guests-page') ) return;
	
	// Prepare layout options.
	var options = {
	  autoResize: true, // This will auto-update the layout when the browser window is resized.
	  container: $('#insta-container'), // $('.col1_2:eq(0) .innercol'), // Optional, used for some extra CSS styling
	  offset: 20, // Optional, the distance between grid items
	  itemWidth: $('.insta').width() // Optional, the width of a grid item
	  ,align: 'left'
	  ,autoResize: false
	};
	// Get a reference to your grid items.
	var handler = $('#instagram div.insta');
	// Call the layout function
	handler.wookmark(options);
	$('#instagram').removeClass( 'loading' );
}


function loadHomeInstagram(){
	var insta_container = $("#instagram"), insta_next_url;
	/* init */
	insta_container.instagram({
	hash: 'ipanemainn',
	accessToken: '2108104116.5b9e1e6.79d61c9cfbf040ee86ddf4820719ad95'
	, userId: 2108104116  /* ferdydurque from http://jelled.com/instagram/lookup-user-id */
	, clientId : '27eb9f37b48647768524ded35db73f1c'
	, show : 20
	, append: false
	, image_size: 'standard_resolution'
	, onComplete : function (photos, data) {
			appendPhotosHome( data.data );
			loading = false;
		}
	});
}

function appendPhotosHome( arr ){
	//console.log( arr )
	var cnts = [1,2,3]
	for ( var i=0; i<3; i++ ){
		var stream = '';
		var o = arr[i];
		var lnk = o.link;
		var txt = o.caption ? o.caption.text : '';
		stream += '';
			stream += '<img src="' + o.images.standard_resolution.url + '"/>';
			//stream += '<div class="photo-txt"><div class="cap">“</div><div class="dsct-italic"><p>' + txt + '</p></div><div class="name">— ' + o.user.username + '</div></div>';
		stream += '';
		$('#gr'+cnts[i] + ' div.placeholder .loader').append( stream );
		$('#gr'+cnts[i] + ' div.placeholder').removeClass( 'loading' );
	}	
};












/* CONTACT PAGE *************************************
function initContact(){
	$('#container-map').imagesLoaded( function(){
		$('.marker').each(function(index, element) {
           $(this).css( {
				left:$(this).position().left - $(this).width()/2,
		   		top:$(this).position().top - $(this).height()		
			}).transition({opacity:1}, 337, 'easeOutCircle'); 
        });
		$('.indication').hover( function(e){
				$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
			}, 
			function(e){
				$('.map-marker:eq(' + $(this).attr('rel') + ')').removeClass( 'selected' ); 			
			} );

		$('.map-marker').hover( function(e){
				setBocadillo( $(this).attr('rel'), true )
			}, 
			function(e){
				setBocadillo( $(this).attr('rel'), false )
			} );
			
		$('.map-marker-big').hover( function(e){
				setBocadillo( -1, true )
			}, 
			function(e){
				setBocadillo( -1, false )
			} );
			
		var $win = $( '#win-marker' );
			
		// ipad
		$('.map-marker').click( function(){
			setBocadillo( $(this).attr('rel'), true );
		} );
		$('.map-marker-big').click( function(){
			setBocadillo( -1, true );
		} );	
		$win.click( function(){ $win.hide(); });	
		
		$('.row .indication').click( function(){
			$('.marker').removeClass( 'selected' );
			$('.indication').removeClass( 'selected' );
			$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
			$(this).addClass( 'selected' );
		})		
		
		function setBocadillo( j, b ){
			var i = parseInt( j );
			var $marker = i < 0 ? $('.map-marker-big') : $('.map-marker:eq(' + i + ')');
			var $map = $('#map');
			
			if ( b ){
				var html = '<ul><li>';
				if ( $marker.attr('data-img') ){
					if ( $marker.attr('data-img') != '' ){
						html += '<img class="map-window" src="'+ $marker.attr('data-img') +'"></li><li>';
					}
				}
				html += (i < 0 ? '<div class="indication" rel="-1"><div class="i-title">Ipanema Inn</div><div class="i-text"><p><strong></strong></p></div></div>' : $('.row .indication:eq(' + i + ')').clone()[0].outerHTML );
				html += '</li></ul>';
				
				
				$win.empty().html( html );
				
				var left = $marker.position().left - 40;
				if ( left + $win.width() > $map.width() ){
					left = $map.width() - $win.width() - 40;
				}
				var top = $marker.position().top - $win.height() - 40;
				if ( top < 20 ){
					top = 20;
					if ( $marker.position().left > $map.width()/2 ){
						// go left the marker
						left = $marker.position().left - $win.width() - 40;
					}
					else{
						// go right the marker
						left = $marker.position().left + 40;
					}
				}
				$win.css({left: left
					,top: top
				});
				$win.show();
			}
			else{
				$win.hide().empty();
			}
		}
		resizeMap();
	});
}

function resizeMap(){
	if ( $('#map').length > 0 ){
		var w = $window.width();
		var p;
		if ( w <= 480 ){
			//console.log( 'mobile' )
		}
		else if ( w <= 1024 ){
			//console.log( 'ipad' )
			p = 748/996;
		}
		else{
			//console.log( 'desktop' )
			p = 1;
		}
		if ( p ){
			$('.marker').each(function(index, element) {
				var coor = $(this).attr('data-rel').split( ',' );
				$(this).css( {
					left: parseInt(coor[0])*p,
					top: parseInt(coor[1])*p		
				});
			   $(this).css( {
					left:$(this).position().left - $(this).width()/2,
					top:$(this).position().top - $(this).height()/2		
				}); 
			});
		}
	}
}

function initMaps(){
	
	if ( ! document.getElementById("google-map") ) return;
	
	
	var ip = document.title.indexOf('Ipanema') != -1;
	
	
	var ipanema = new google.maps.LatLng(-22.985482, -43.207283);
	var arpoador = new google.maps.LatLng(-22.988501, -43.191807);
	var latlang = ip ? ipanema : arpoador;
	var zoom = ip ? 14 : 15;
	
	
	var mapOptions = {
	  center: latlang,
	  zoom: zoom,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	  
	  ,mapTypeControl: false
	};
	var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);
		
	var styles = [
	  {
		stylers: [
		  { saturation: -70 }
		]
	  }
	];
	map.setOptions({styles: styles});


	
	var beachMarker = new google.maps.Marker({
      position: latlang,
      map: map,
      icon: TEMPLATE_URL + '/imgs/mapicon.png'
  });
  
}
*/