
var ismobileLayout = false;

var offset,$htmlBody,$window;
var barFixedInPages = true;
var device;
var dategeral;
$(document).ready(function(e) {
	$htmlBody = $('html,body');
	offset  = $('header').height();
	$window   = $(window);	

	if ( $('body').hasClass( 'home' ) ) {
		initHome();
	}
  else{}
  
  $('.slideshow').slick({
    centerMode: true,
    centerPadding: '0px',
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          dots: true,
          centerMode: true,
          centerPadding: '0px',
          slidesToShow: 1
        }
      }
    ]
  });

	
	if ( $('#main').hasClass('rooms-page') ) initRooms();
	if ( $('#main').hasClass('cityandsea-page') ) initCityAndSea();
	if ( $('#main').hasClass('guests-page') ) initGuests();
	if ( $('#main').hasClass('contact-page') || $('body').hasClass( 'home' ) ) initContact();

	//initBookNowBar();
	
	// city and the sea fix menu selection when category and single
	if ( $('body').hasClass( 'category' ) || $('body').hasClass( 'single' ) ){
		// pt
		$( '#menu-item-114' ).addClass( 'current_page_item' );
		// en
		$( '#menu-item-123' ).addClass( 'current_page_item' );
	} 
				
	// RESIZE
  	$window.resize( function(e){
		onResize(e)
	});
	onResize();
	
	if ( ismobileLayout ){
		$('#mobile-wrap-container').scroll( onUserScroll );
	}
	else{
		$window.scroll(  onUserScroll );
	}

	initMobileMenu();
	
	initBookNowBar();

	//initMaps()
});
/* 
$(window).load(function(e){
	// date picker
	
	initBookNowBar();

});*/


function initMobileMenu(){
	$('#mobile-icon').click(function(e){
		toggleMobileMenu()
	});
	$('#drawer').data('open', false);
}
function toggleMobileMenu(){
	var toOpen = !$('#drawer').data('open');
	$('#drawer').animate({left:toOpen?0:-200}, 477, 'easeInOutQuad' );
	$('#mobile-wrap').animate({left:toOpen?200:0}, 377, 'easeInOutQuad' );
	$('#drawer').data('open', toOpen);
}

function onResize(e){
	
	switch ( $('#mediaquery').css('float') ){
		case 'left':
			device = 'ipad';
		break;
		case 'right':
			device = 'iphone';
		break;
		default:
			device = 'desktop';
	}
	//console.log( 'device = ' + device + ', float = ' + $('#mediaquery').css('float') );
	var w = $window.width();
	var h = $window.height();
	var ww = $('#header-container').width();
	var margin = parseInt( $('.margin-wide').css('margin-left') );
	

	// mobile!
	if ( ismobileLayout ){
		$('#mobile-wrap').width(w);
		$('#mobile-wrap').height(h);
	}




	/* BOOK NOW BAR FIX!!!!! */
	if ( $('body').hasClass('home') ){
		$('#book-now-bar').width(w-margin*2);
	}
	else{
		margin = parseInt( $('.page-wide').css('margin-left') );
		$('#book-now-bar').width(w-margin*2);
		if ( barFixedInPages ){
			$( '#book-now-bar' ).addClass( 'bar-locked-page' );
			$( '#page-wrapper' ).css( 'margin-top', $( '#book-now-bar' ).height() + $('#masthead').height() );	
		}
	}
	resizeGrid();
	// layoutInstagram();
	if ( $('#main').hasClass('guests-page') ) resizeGuests();
	if ( $('body').hasClass('home') || $('#main').hasClass('contact-page')  ) resizeMap();
}

var userclick = false;
function scrollMenu( id ){
	var time = 577;
	userclick = true;
	var toy = id == '#home' ? 0 : $(id).offset().top-offset;
	$htmlBody.animate({scrollTop: toy }, time, 'easeInOutCirc', function(){
		setTimeout( function(){
			userclick = false;
		}, 200 );
	} );  
}




function initHome(){
	//initSlideShow();
	initSlideShow( 4000, undefined, [1500,655] );
	// initHomeRooms();
	// loadHomeInstagram();
}

function initRooms(){
	initSlideShow();
	initLayoutRooms()
}

var fromDatePicker, toDatePicker;
function initBookNowBar(){
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var $btn = $('#book-now-btn');
	var hotel = 99223;
	var locale = $btn.attr('data-locale');
	//var iniUrl = url + '?Hotel=' + hotel + '&Chain=' + chain + '&locale=' + locale;
	

	var fromDate = now;
	// next day
	var toDate = new Date( now ); 
	toDate.setDate( toDate.getDate() + 1 );
	
	var dowNames = $('html').attr('lang') == 'en-US' ? [ 'S', 'M', 'T', 'W', 'T', 'F', 'S' ] : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ];
	var monthNames = $('html').attr('lang') == 'en-US' ? [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ] : [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ];
	var shortMonthNames = $('html').attr('lang') == 'en-US' ? [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ] : [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ];
	
	$('#book-now-bar').show();
	fromDatePicker = $('#from').glDatePicker( {
		cssName: 'flatwhite'
		,dowNames: dowNames
		,monthNames: monthNames
		,width:260
		,prevArrow:'←'
		,nextArrow:'→'
		,hideOnClick: true
		, onClick: (function(el, cell, date, data) {
			
			// Salva a data em formato correto para envio da API
			$(el).data('api', moment(date).format("MM/DD/YYYY")).change();

			if ( date.getTime() < now.getTime() ){
				fromDatePicker.options.selectedDate = new Date(); //fromDate;
				fromDatePicker.render();
				el.val( el.attr('data-default') );
			}else{
        		var d = date.getDate()+ ' ' + shortMonthNames[date.getMonth()] + ', ' + date.getFullYear();	
				el.val(d)

				fromDate = date;
				if (toDatePicker.options.selectedDate.getTime() <= date.getTime() ) {				
					toDate = new Date( date ); 
					toDate.setDate( toDate.getDate() + 1);

					toDatePicker.options.selectedDate = toDate;
					toDatePicker.selectedDate = toDate;

					toDatePicker.options.firstDate = toDate;
					toDatePicker.firstDate = toDate;

					toDatePicker.render();
					// var d2 = formatdate2(("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getDate() + '/' + date.getFullYear());		

        			var d2 = toDatePicker.options.selectedDate.getDate()+ ' ' + shortMonthNames[toDatePicker.options.selectedDate.getMonth()] + ', ' + toDatePicker.options.selectedDate.getFullYear();				
					$("#to").val(d2);
					$("#to").focus();
	        		
				}
				toDatePicker.show()	
			}
    	})
		, onShow: function(calendar) { 
			setTopPosition( calendar, fromDatePicker )
		}

	}).glDatePicker(true);

	toDatePicker = $('#to').glDatePicker({
		cssName: 'flatwhite'
		,dowNames: dowNames
		,monthNames: monthNames
		,width:260
		,prevArrow:'←'
		,nextArrow:'→'
		, onClick: (function(el, cell, date, data) {
			// Salva a data em formato correto para envio da API
			$(el).data('api', moment(date).format("MM/DD/YYYY")).change();

				// console.log(date.getTime());
				dategeral = date;
			if ( date.getTime() > fromDate.getTime() ){						
        		// var d = formatdate2(("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getDate() + '/' + date.getFullYear());	
        		
        		var d = date.getDate()+ ' ' + shortMonthNames[date.getMonth()] + ', ' + date.getFullYear();	
				el.val(d)
				toDate = date;
			}
			
    	})
		, onShow: function(calendar) { 
			setTopPosition( calendar, toDatePicker );
		}
	}).glDatePicker(true);
	
	function setTopPosition( calendar, which ){
		
		if ( $( '#book-now-bar').hasClass('bar-locked') ){
			calendar.css( {top: ( $(window).scrollTop() + $( '#book-now-bar').position().top + 43 + which.options.calendarOffset.y ) + 'px'} ); 
		}
		else{
			var el = $('#from');
			var elPos = el.offset();
			calendar.css(
			{
				top: (elPos.top + el.outerHeight() + which.options.calendarOffset.y ) + 'px',
				//left: (elPos.left + options.calendarOffset.x) + 'px'
			});
		}
		calendar.show(); 
	};
	
	
	// select
	$('#adult').change(function(e) {
        $('#uniform-adult span').html(e.currentTarget.value)
    });

    initBooking()
}

function initBooking(){
	// mobile
	$('.book-now-mobile').click( function(e){
		//e.preventDefault();

		window.open( getiniUrl() );
	});
	// desktop
	$('a#book-now-bar-main').click( function(e){
		e.preventDefault();

		var $btn = $('#book-now-btn');
		// var iniUrl = getiniUrl();
		var datein = $('#from').data('api');
		var dateout = $('#to').data('api');
		var locale = $btn.attr('data-locale');
		var adults = $('#adult').val();

		var iniUrl = "https://booking.ihotelier.com/istay/istay.jsp?hotelid=99221&adults=" + adults +"&dateout=" + dateout +"&datein=" + datein + "&languageid=" + locale;
			window.open(iniUrl);
		// console.log(iniUrl);
		
		// $('.bg-modal').show();		
		
		// setInterval(function(){ 
		// 	window.location.replace(iniUrl);
		// 	(iniUrl); 
		// }, 8000);
		
				
		
		// window.open( iniUrl );
	})
}
function getiniUrl(){
	var $btn = $('#book-now-btn');
	var url = $btn.attr('data-url');
	var hotel = $btn.attr('data-hotel');
	//var chain = $btn.attr('data-chain');
	var locale = $btn.attr('data-locale');
	//var iniUrl = url + '?Hotel=' + hotel + '&Chain=' + chain + '&locale=' + locale;
	var iniUrl = url + '?hotelid=' + hotel + '&languageid=' + locale;
	return iniUrl	
}
function formatdate(s){
	var locale = $('#book-now-btn').attr('data-locale');	
	var a = s.split('/');
	var dd, mm, yyyy;
	var str = '';
	for ( var i=0; i<a.length; i++){
		var v = a[i];
		if ( i < 2 ){
			if ( v.length === 1 ){
				v = '0' + v;
			}
			if ( i === 0 ){
				mm = v;
			}
			else{
				dd = v;
			}
		} else{
			yyyy = v
		}
	}

	return encodeURIComponent( locale == 1 ? ( mm + '/' + dd + '/' + yyyy ) : ( dd + '/' + mm + '/' + yyyy ) );
	//return encodeURIComponent( locale === 'en-US' ? ( mm + '/' + dd + '/' + yyyy ) : ( dd + '/' + mm + '/' + yyyy ) );
}

function formatdate2(s){
	var locale = $('#book-now-btn').attr('data-locale');	
	var a = s.split('/');
	var dd, mm, yyyy;
	var str = '';
	var posYear = 0;
	for ( var i=0; i<a.length; i++){
		var v = a[i];
		if ( i < 2 ){
			if ( v.length === 1 ){
				v = '0' + v;
			}
			if ( i === 0 ){
				mm = v;
			}
			else{
				dd = v;
			}
		} else{
			posYear = i;
			yyyy = v;
		}
	}
	
	if (navigator.userAgent.indexOf('Win') > 0 ){
		return (locale == 4 ? ( mm + '/' + dd + '/' + yyyy ) : ( dd + '/' + mm + '/' + yyyy ) );
	}else{
		var ret = "";
		if( locale == 1) {
			ret = mm + '/' + dd + '/' + yyyy;
		}else{
			ret = dd + '/' + mm + '/' + yyyy;
		}
		return ret;
	}
}


function initCityAndSea(){
	var tot = $('#slide-txt ul li').length;
	initSlideShow( 8000 , function(n){
		$('#slide-txt ul').transition( {left: n == tot ? 0 : -n*($('#slide-txt ul li').width() + parseInt($('#slide-txt ul li').css('margin-right'))) }, 700, 'easeInOutCirc'  );	
	} );
	// categories
	// link to all categories
	$('ul#categories li:eq(0) a').attr('href',  $('ul#categories li:eq(0) a').attr('href') + 'city-sea' );
	if ( $('#categories li.current-cat').length == 0 ){
		$('ul#categories li:eq(0)').addClass( 'current-cat' );
	}
	
	// pagination
	$('a.pagination-links').click(function(e) {
	   	e.preventDefault();
		$(this).parents('form').submit();	
	});
	// grid
	if ( $('#grid-posts').length > 0 ){
		
		
		$('#tiles').css('position', 'relative');
			
      $('#tiles').imagesLoaded(function() {
        // Prepare layout options.
		resizeGrid();
      });
	}
}

function resizeGrid(){
	// Get a reference to your grid items.
	var handler = $('#tiles li');

	if ( handler.length == 0 ) return;
	
	var offset = $(window).width() > 480 ? 50 : 0;
	var w = $(window).width() > 480 ? 200 : 300;
	
	var options = {
	  autoResize: false, // This will auto-update the layout when the browser window is resized.
	  container: $('#grid-posts'),// Optional, used for some extra CSS styling
	  offset: offset, // Optional, the distance between grid items
	  itemWidth: w // Optional, the width of a grid item
	  
	  ,align: 'left'
	};
	// Call the layout function.
	handler.wookmark(options);

	setTimeout( function(){
		handler.wookmark();
	}, 77 );

}

// USER SCROLL
function onUserScroll(){  
	var top =  ismobileLayout  ? $('#mobile-wrap-container').scrollTop() : $window.scrollTop();

	//console.log( top )

	var offset = parseInt ( $('#main').css('margin-top') );
	
	
	if ( device != 'desktop' ) return;
	
	if ( $('header').css( 'position' ) == 'fixed' ){ 
	
		if ( $('body').hasClass('home') ){	
			if ( top > $('#home-header').height()- $('header').height() ){ // slideshow height + difference between headers
				if ( !$( '#book-now-bar').hasClass('bar-locked') ){
					
					$("#slides").css( 'margin-top', $( '#book-now-bar' ).height() ); 
					$( '#book-now-bar' ).addClass( 'bar-locked' );
					
					$('#logo-small').stop().transition({opacity:1}, 1377, 'easeInQuad' );
					$('#logo-small').css('display','block');
					$('#lang-switch').stop().transition({opacity:1}, 1377, 'easeInQuad' );
					$('#lang-switch').css('display','block');
					
					
					//onResize()
					
					//console.log( $( '#book-now-bar').width() )
				}
			}
			else{
				if ( $( '#book-now-bar').hasClass('bar-locked') ){
					
					$( '#book-now-bar' ).removeClass( 'bar-locked' );
					$("#slides").css( 'margin-top', 0 ); 
					
					$('#logo-small').stop().transition({opacity:0}, 277 ,'easeInQuad' , function(){
						$(this).css('display','none');
					});
					$('#lang-switch').stop().transition({opacity:0}, 277 ,'easeInQuad' , function(){
						$(this).css('display','none');
					});
					
					//console.log( $( '#book-now-bar').width() )
				}
			}
		}
		else{
			if ( top > $('#book-now-bar').height() ){
				if ( !barFixedInPages ){				
					if ( $('#line-bottom').hasClass('invisible') ){
						$('#line-bottom').removeClass('invisible')
						$('#book-now-btn').stop().transition({opacity:1}, 1377, 'easeInQuad' );
						$('#book-now-btn').css('display','block');
					}
				}
			}
			else{
				if ( !barFixedInPages ){
					if ( !$('#line-bottom').hasClass('invisible') ){
						$('#line-bottom').addClass('invisible')
						$('#book-now-btn').stop().transition({opacity:0}, 277, 'easeInQuad', function(){
							$(this).css('display','none');
						} );
					}
				}
			}
		}
	
		if ( fromDatePicker ) fromDatePicker.hide();
		if ( toDatePicker ) toDatePicker.hide();
		
		// menu pages
		if ( $('#main').hasClass('rooms-page') ){
	
			if ( barFixedInPages ) offset += $('#book-now-bar').height();
			
			if ( $('#nav-rooms-header').css( 'position') != 'fixed' ){
				if ( top > Math.ceil( $('#nav-rooms-header').offset().top - offset ) + 1 ){
					$('#nav-rooms-header').css( 'position', 'fixed' );
					$('#nav-rooms-header').css( 'top', offset -1 );
					$('#slideshow').css( 'margin-top', $('#nav-rooms-header').height() );
				}
			}
			else{
				if ( top < $('#slideshow').offset().top - $('#nav-rooms-header').height() - offset ){
					$('#nav-rooms-header').css( 'position', 'relative' );
					$('#nav-rooms-header').css( 'top', 'auto' );
					$('#slideshow').css( 'margin-top', 0 );
				} 
			}
		}
	}
}

function initSlideShow( speed, callback, sizes ){
	//console.log( sizes );

	var w = 996;
	var h = 498;
	if ( sizes !== undefined ){
		w = sizes[0];
		h = sizes[1];
	}	
	var autoplay = $(".slides img").length > 1;
	$(".slides").slidesjs( {
		width: w,
		height: h,
		callback: {
		  loaded: function(number) {
			// Use your browser console to view log
			//console.log('SlidesJS: Loaded with slide #' + number);
		  },
		  start: function(number) {
			// Use your browser console to view log
			//console.log('SlidesJS: Start Animation on slide #' + number);
			if ( callback ) callback(number)
		  },
		  complete: function(number) {
			// Use your browser console to view log
			//console.log('SlidesJS: Animation Complete. Current slide is #' + number);
		  }
		}
		,navigation: false
		,pagination: {
			 effect: "fade"
		}
		,play: {
		  active: false,
			// [boolean] Generate the play and stop buttons.
			// You cannot use your own buttons. Sorry.
		  effect: "fade",
			// [string] Can be either "slide" or "fade".
		  interval: speed || 4000,
			// [number] Time spent on each slide in milliseconds.
		  auto: autoplay,
			// [boolean] Start playing the slideshow on load.
		  swap: false,
			// [boolean] show/hide stop and play buttons
		  pauseOnHover: true,
			// [boolean] pause a playing slideshow on hover
		  restartDelay: 500
			// [number] restart delay on inactive slideshow
		}
		,effect: {
			  slide: {
				// Slide effect settings.
				speed: 200
				  // [number] Speed in milliseconds of the slide animation.
			  },
			  fade: {
				speed: 700,
				  // [number] Speed in milliseconds of the fade animation.
				crossfade: true
				  // [boolean] Cross-fade the transition.
			  }
		}
	});

	if ( !autoplay ){
		$(".slides a").addClass('nop');
	}

}

function initLayoutRooms(){
	if ( $('#layout-images img').length > 1 ){
		$('#layout-images').imagesLoaded( function( $imgs ){
			var h = 0; 
			var maxi = 0;
			$imgs.each( function ( i, el ){
				if ( i > 0 ) {
					$(this).css( 'top', - h );
					$(this).css( 'opacity', 0 );
				}
				h += $(this)[0].height;
				if ( $(this)[0].height > maxi ) maxi = $(this)[0].height;
			} );
			$('#layout-images').height(maxi);
			
			$('#layout li').each( function(i, el){
				$(this).click( function(e){
					setLayout( i );
				} );
				if ( i == 0 ) $(this).addClass('active');
			} );
			$(this).show();
		});
		function setLayout( i ){
			$('#layout li').each( function(j, el){
				if ( j == i ) {
					$(this).addClass('active');
					$('#layout-images img:eq('+ j +')').transition({ opacity:1}, 400, 'easeOutQuad' );
				}
				else{
					$(this).removeClass('active');
					$('#layout-images img:eq('+ j +')').transition({ opacity:0}, 200, 'easeInQuad' );
				}
			});
		}
	}
	else
		$('#layout-images').show();
}

