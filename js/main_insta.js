
$(document).ready(function(e) {
	if ( $('#main').hasClass('guests-page') ) initGuests();
});


function initHome(){
	//initSlideShow();
	initSlideShow( 4000, undefined, [1500,655] );
	initHomeRooms();
	loadHomeInstagram();
}
function initGuests(){
	
	
	/* user photos */
	var tot = $('#slide-txt ul li').length;
	initSlideShow( 8000 , function(n){
		//$('#slide-txt ul').transition( {left: n == tot ? 0 : -n*($('#slide-txt ul li').width() + parseInt($('#slide-txt ul li').css('margin-right'))) }, 700, 'easeInOutCirc'  );	
		$('#slide-txt ul').transition( {opacity: 0 }, 200, 'easeOutCirc', function(){
			$(this).css('left',n == tot ? 0 : -n*($('#slide-txt ul li').outerWidth() + parseInt($('#slide-txt ul li').css('margin-right'))) );
			$(this).transition( {opacity: 1 }, 300, 'easeOutCirc' );
		}  );	
	} );
	
	
	
	/* instagram */
	var loading = true;
	var insta_container = $("#instagram"), insta_next_url;
	/* init */
	
	insta_container.instagram({
	  userId: 1606081558  /* ferdydurque from http://jelled.com/instagram/lookup-user-id */
	, clientId : 'eb05fecc61024f25ab7df656a7ff58db'
	, accessToken: '1606081558.eb05fec.16fa79c990294bd7a57273c71d4a9d26'
	//, hash: 'hotelarpoador'
	, show : 18
	, append: false
	, image_size: 'standard_resolution'
	, onComplete : function (photos, data) {
			//console.log(data);
			insta_next_url = data.pagination.next_url;
			appendPhotos( data.data );
			loading = false;
		}
	});
	function loadNextInstagram(){	
		if ( !loading ){
		  loading = true;
		  insta_container.instagram({
			  next_url : insta_next_url
			, show : 11
			, append: false
			, onComplete : function(photos, data) {
			  insta_next_url = data.pagination.next_url;
			  appendPhotos( data.data );
			  if ( insta_next_url ){
				   loading = false;
			  }
			  else{
			    $('a.instamore').hide(0);  
			  }
			  
			}
		  })
		}       
	}; 
	/* append photos */
	function appendPhotos( arr ){
		//console.log( arr )
		var stream = '';
		for ( var i=0; i<arr.length; i++ ){
			var o = arr[i];
			var lnk = o.link;
			var txt = o.caption ? o.caption.text : '';
			stream += '<div class="insta">';
				stream += '<a href="'+ lnk +'" target="_blank"><img src="' + o.images.standard_resolution.url + '" class="pt"/></a>';
				//stream += '<div><img src="' + o.user.profile_picture + '" class="pu"/><span class="sb">' + o.user.username + '</span><br/>' + txt + '</div>';
				//stream += '<center><a href="'+ lnk +'" target="_blank" class="like">' + o.likes.count + ' Likes</a></center>';
				stream += '<div><span class="sb">— <a href="http://instagram.com/'+ o.user.username +'" target="_blank">' + o.user.username + '</a></span></div>';
			stream += '</div>';
		}	
		
		//console.log( stream )
		insta_container.append( stream );
		
		insta_container.imagesLoaded(function() {
			layoutInstagram()
		});
	};
	$('a.instamore').click(function(e) {
        e.preventDefault();
		loadNextInstagram();
    });
	
	
	// tweets
	var config = {
	  "id": '641622786352488448',
	  "domId": '',
	  "maxTweets": 5,
	  "enableLinks": true,
	  "showUser": true,
	  "showTime": true,
	  "dateFunction": '',
	  "showRetweet": false,
	  "customCallback": handleTweets,
	  "showInteraction": false
	};

	function handleTweets(tweets){
		var x = tweets.length;
		var n = 0;
		var element = document.getElementById('tweets');
		var html = '<ul>';
		while(n < x) {
		  html += '<li>' + tweets[n] + '</li>';
		  n++;
		}
		html += '</ul>';
		element.innerHTML = html;
		
		$('#tweets').removeClass( 'loading' );
	}

	twitterFetcher.fetch(config);
}

function resizeGuests(){
	
}


function initHomeRooms(){
	$('.grid-rooms').imagesLoaded( function(){
		// set scrolls
		$('.grid-slider').each(function(index, element) {
            if ( $(this).hasClass( 'orizontal' ) ){
				$(this).width( 4*$(this).parents('.room-img-grid').width() );
			};
			// set current slide
			var n = parseInt($(this).attr('rel'));
			setCurrentSlide( n , index );
        });
		$('.room-img-grid div.over').each(function(index, element) {
			var $this = $(this);
            $this.css({ 'line-height': $(this).height() + 'px'});
			$this.hover( 
				function(e){
					$this.stop().transition({ opacity:1}, 200, 'easeOutQuad' );
					setCurrentSlide( parseInt($(this).attr('rel')) , -1 );
				}, 
				function(e){
					$this.stop().transition({ opacity:0}, 400, 'easeInQuad' );	
					setCurrentSlide( -1 , -1 )				
				} 
			);
        });
		
		function setCurrentSlide( n, index ){
			//console.log( '>> setCurrentSlide n = ' + n + ', index = ' + index  );
			var $el = $('.grid-slider');
			if ( index != -1 ){
				$el = $('.grid-slider:eq(' + index + ')' );
			}
			var qual = n;
			$el.each( function( i, element ){
				if ( n == -1 ){
					qual = parseInt($(this).attr('rel'));
				}
				if ( $(this).hasClass( 'orizontal' ) ){
					$(this).stop().transition({ left: - qual*$(this).parents('.room-img-grid').width() }, 270 + i*37, 'easeInOutCirc' );
				}
				else{
					$(this).stop().transition({ top: - qual*$(this).parents('.room-img-grid').height() }, 270 + i*37, 'easeInOutCirc' );
				}
			});
		};

	});
};



function layoutInstagram(){
	if ( ! $('#main').hasClass('guests-page') ) return;
	
	// Prepare layout options.
	var options = {
	  autoResize: true, // This will auto-update the layout when the browser window is resized.
	  container: $('#insta-container'), // $('.col1_2:eq(0) .innercol'), // Optional, used for some extra CSS styling
	  offset: 20, // Optional, the distance between grid items
	  itemWidth: $('.insta').width() // Optional, the width of a grid item
	  ,align: 'left'
	  ,autoResize: false
	};
	// Get a reference to your grid items.
	var handler = $('#instagram div.insta');
	// Call the layout function
	handler.wookmark(options);
	$('#instagram').removeClass( 'loading' );
}


function loadHomeInstagram(){
	var insta_container = $("#instagram"), insta_next_url;
	/* init */
	insta_container.instagram({
	//hash: 'hotelarpoador'
	 accessToken: '1606081558.eb05fec.16fa79c990294bd7a57273c71d4a9d26'
	, userId: 1606081558  /* ferdydurque from http://jelled.com/instagram/lookup-user-id */
	, clientId : 'eb05fecc61024f25ab7df656a7ff58db'
	//, accessToken :  '4cc0abebed48494ea5ee0378a7f855c3'
	, show : 20
	, append: false
	, image_size: 'standard_resolution'
	, onComplete : function (photos, data) {
			appendPhotosHome( data.data );
			loading = false;
		}
	});
}

function appendPhotosHome( arr ){
	//console.log( arr )
	try{
	var cnts = [1,2,3]
	for ( var i=0; i<3; i++ ){
		var stream = '';
		var o = arr[i];
		var lnk = o.link;
		var txt = o.caption ? o.caption.text : '';
		stream += '';
			stream += '<img src="' + o.images.standard_resolution.url + '"/>';
			//stream += '<div class="photo-txt"><div class="cap">“</div><div class="dsct-italic"><p>' + txt + '</p></div><div class="name">— ' + o.user.username + '</div></div>';
		stream += '';
		$('#gr'+cnts[i] + ' div.placeholder .loader').append( stream );
		$('#gr'+cnts[i] + ' div.placeholder').removeClass( 'loading' );
	}	
	}catch(e){}
};












/* CONTACT PAGE *************************************
function initContact(){
	$('#container-map').imagesLoaded( function(){
		$('.marker').each(function(index, element) {
           $(this).css( {
				left:$(this).position().left - $(this).width()/2,
		   		top:$(this).position().top - $(this).height()		
			}).transition({opacity:1}, 337, 'easeOutCircle'); 
        });
		$('.indication').hover( function(e){
				$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
			}, 
			function(e){
				$('.map-marker:eq(' + $(this).attr('rel') + ')').removeClass( 'selected' ); 			
			} );

		$('.map-marker').hover( function(e){
				setBocadillo( $(this).attr('rel'), true )
			}, 
			function(e){
				setBocadillo( $(this).attr('rel'), false )
			} );
			
		$('.map-marker-big').hover( function(e){
				setBocadillo( -1, true )
			}, 
			function(e){
				setBocadillo( -1, false )
			} );
			
		var $win = $( '#win-marker' );
			
		// ipad
		$('.map-marker').click( function(){
			setBocadillo( $(this).attr('rel'), true );
		} );
		$('.map-marker-big').click( function(){
			setBocadillo( -1, true );
		} );	
		$win.click( function(){ $win.hide(); });	
		
		$('.row .indication').click( function(){
			$('.marker').removeClass( 'selected' );
			$('.indication').removeClass( 'selected' );
			$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
			$(this).addClass( 'selected' );
		})		
		
		function setBocadillo( j, b ){
			var i = parseInt( j );
			var $marker = i < 0 ? $('.map-marker-big') : $('.map-marker:eq(' + i + ')');
			var $map = $('#map');
			
			if ( b ){
				var html = '<ul><li>';
				if ( $marker.attr('data-img') ){
					if ( $marker.attr('data-img') != '' ){
						html += '<img class="map-window" src="'+ $marker.attr('data-img') +'"></li><li>';
					}
				}
				html += (i < 0 ? '<div class="indication" rel="-1"><div class="i-title">Ipanema Inn</div><div class="i-text"><p><strong></strong></p></div></div>' : $('.row .indication:eq(' + i + ')').clone()[0].outerHTML );
				html += '</li></ul>';
				
				
				$win.empty().html( html );
				
				var left = $marker.position().left - 40;
				if ( left + $win.width() > $map.width() ){
					left = $map.width() - $win.width() - 40;
				}
				var top = $marker.position().top - $win.height() - 40;
				if ( top < 20 ){
					top = 20;
					if ( $marker.position().left > $map.width()/2 ){
						// go left the marker
						left = $marker.position().left - $win.width() - 40;
					}
					else{
						// go right the marker
						left = $marker.position().left + 40;
					}
				}
				$win.css({left: left
					,top: top
				});
				$win.show();
			}
			else{
				$win.hide().empty();
			}
		}
		resizeMap();
	});
}

function resizeMap(){
	if ( $('#map').length > 0 ){
		var w = $window.width();
		var p;
		if ( w <= 480 ){
			//console.log( 'mobile' )
		}
		else if ( w <= 1024 ){
			//console.log( 'ipad' )
			p = 748/996;
		}
		else{
			//console.log( 'desktop' )
			p = 1;
		}
		if ( p ){
			$('.marker').each(function(index, element) {
				var coor = $(this).attr('data-rel').split( ',' );
				$(this).css( {
					left: parseInt(coor[0])*p,
					top: parseInt(coor[1])*p		
				});
			   $(this).css( {
					left:$(this).position().left - $(this).width()/2,
					top:$(this).position().top - $(this).height()/2		
				}); 
			});
		}
	}
}

function initMaps(){
	
	if ( ! document.getElementById("google-map") ) return;
	
	
	var ip = document.title.indexOf('Ipanema') != -1;
	
	
	var ipanema = new google.maps.LatLng(-22.985482, -43.207283);
	var arpoador = new google.maps.LatLng(-22.988501, -43.191807);
	var latlang = ip ? ipanema : arpoador;
	var zoom = ip ? 14 : 15;
	
	
	var mapOptions = {
	  center: latlang,
	  zoom: zoom,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	  
	  ,mapTypeControl: false
	};
	var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);
		
	var styles = [
	  {
		stylers: [
		  { saturation: -70 }
		]
	  }
	];
	map.setOptions({styles: styles});


	
	var beachMarker = new google.maps.Marker({
      position: latlang,
      map: map,
      icon: TEMPLATE_URL + '/imgs/mapicon.png'
  });
  
}
*/