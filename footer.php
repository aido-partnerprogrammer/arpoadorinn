<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
     <div class="clearfix"></div>

	</div><!-- #main .wrapper -->
      
	<footer class="ext-wrap section">
        <hr/>
        		
        <div class="row clearfix">        
        	<div class="col3">
            	<div class="book-now-mobile btn"><?php the_field( 'texto_botao', 'option' ); ?></div>
							<?php
								wp_nav_menu(array(
									'theme_location' => 'nav-footer',
									'menu_class' => false,
									'menu_id' => 'footer-menu',
									'container' => false,
									'walker' => false
								));
								/*
							?>
            	<ul id="footer-menu">
                	<li>
										<a href="<?php echo get_bloginfo( 'url' );?>"><?php echo get_bloginfo( 'name' );?></a>
									</li>
									<?php wp_list_pages('exclude=6,4,77,82,86,90,773,771&depth=1&title_li='); ?>
                </ul> */ ?>
                <p class="link-other-hotel d-none d-md-block">
                    <a target="_blank" href="<? echo get_field('link_hotel', 'options');?>"><? echo get_field('visit_text', 'options' ); ?> <span class="arrow"></span>  <span><? echo get_field('hotel_name', 'options'); ?></span></a>
                </p>
            </div>
        	<div class="col3 directions">
                <?php
                	$pageContato = get_field( 'localizacao', 'options' );                	
                	echo apply_filters('the_content', get_field( 'endereco', $pageContato));
				?>
            </div>
        	<div class="col3">
            	<h6 class="margin5"><?php the_field( 'redes_sociais_titulo', 'options' ); ?></h6>
                <ul id="follow" class="clearfix">
				      <?php 
                $socials = get_field('social_links', 'options');
                if ($socials) {
                  foreach ( $socials as $social ):
                    if ($social['link']) {
						  ?>
                  <li>
                    <a target="_blank" href="<?php echo $social['link']; ?>">
                      <img src="<?php echo $social['icon']['url']; ?>" />
                    </a>
                  </li>
              <?php
                    }
					        endforeach;
                }
				      ?>
                </ul>

            	<h6 class="margin5"><?php the_field( 'newsletter_title', 'option' ); ?></h6>                
								<!-- Begin MailChimp Signup Form -->
								<div id="mc_embed_signup">
									<form action="https://grupoarpoador.us4.list-manage.com/subscribe/post?u=77379faf7935822496d561718&amp;id=e73560dd04" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
										<div id="mc_embed_signup_scroll">											
											<div class="mc-field-group">
												<label for="mce-EMAIL" class="sr-only">Email Address </label>
												<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
											</div>
											
											<div class="mc-field-group input-group" style="display:none;">
												<strong>Fonte </strong>
												<ul>
													<li><input type="radio" value="1" name="group[68981]" id="mce-group[68981]-68981-0"><label for="mce-group[68981]-68981-0">CADASTROS SITE IPANEMA INN</label></li>
													<li><input checked type="radio" value="2" name="group[68981]" id="mce-group[68981]-68981-1"><label for="mce-group[68981]-68981-1">CADASTROS SITE ARPOADOR</label></li>
												</ul>
											</div>
												<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
												<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_77379faf7935822496d561718_e73560dd04" tabindex="-1" value=""></div>
												<div class="clear">
												 <input type="submit" value="<?php the_field( 'newsletter_submit', 'option' ); ?>" name="subscribe" id="mc-embedded-subscribe" class="btn">
												</div>
										</div>
									</form>
								</div>                              
				<br><br>
                <!--End mc_embed_signup-->
				<h6><?php the_field( 'sac_title', 'option' ); ?></h6>
			    <p><?php the_field( 'sac_tel', 'option' ); ?></p>

            	<h6><?php the_field( 'trabalhe_conosco_texto', 'option' ); ?></h6>
              <?php 
								$link = get_field('trabalhe_conosco_email', 'option');
								if ($link) {	
									echo '<p><a href="'. $link['url'] .'" rel="nofollow" target="'. $link['target'] .'" class="red">'. $link['title'].'</a></p>';
								}
							?>
                
            </div>

            <div class="clearfix"></div>

						<p class="link-other-hotel d-md-none">
							<a target="_blank" href="<? echo get_field('link_hotel', 'options');?>"><? echo get_field('visit_text', 'options' ); ?> <span class="arrow"></span>  <span><? echo get_field('hotel_name', 'options'); ?></span></a>
						</p>


        </div>

        <div class="row clearfix little">        
        	<div class="col3">
        		<?php the_field( 'copyrights', 'option' );?>
            </div>
        	<div class="col3">
            	<span class="dst-italic little">Site by</span>&nbsp;&nbsp;<a href="https://aido.com.br/apresentacao/">AIDO</a>
            </div>
        	<div class="col3">
            </div>

            <div class="clearfix"></div>
        </div>
        
        
        <div class="spacer60"></div>

        <div class="clearfix"></div>

	</footer>

    
    <!--</div>  #mobile-wrap-container -->
</div>  <!-- #mobile-wrap -->
    

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZjBxFScnY08Kd9TWJx1C57JSVkq-06Kk&sensor=false"></script>
    <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery-1.10.1.min.js'; ?>"><\/script>')</script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/moment.min.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.easing.1.3.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/glDatePicker-2.0/glDatePicker.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/slidesjs/jquery.slides.min.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.imagesloaded.min.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.transit-master/jquery.transit.min.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/wookmark/jquery.wookmark.min.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/vendor/twitter/twitterFetcher_v10_min.js'; ?>"></script>    
    <script src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>      
     <script src="<?php echo get_template_directory_uri() . '/js/main.js?'. time(); ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery.instagram/jquery.instagram.js'; ?>"></script>    
    <script src="<?php echo get_template_directory_uri() . '/js/main_insta.js'; ?>"></script>
	<script>
    	$(document).ready(function(){
 
  			$('a.link-tripadvisor').click(function(){
    			window.open(this.href);
    		return false;
  			});
		});
    </script>

    <script>
	/* CONTACT PAGE **************************************/
	function initContact(){
		$('#container-map').imagesLoaded( function(){
			$('.marker').each(function(index, element) {
			   $(this).css( {
					left:$(this).position().left - $(this).width()/2,
					top:$(this).position().top - $(this).height()		
				}).transition({opacity:1}, 337, 'easeOutCircle'); 
			});
			$('.indication').hover( function(e){
					$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
				}, 
				function(e){
					$('.map-marker:eq(' + $(this).attr('rel') + ')').removeClass( 'selected' ); 			
				} );
	
			$('.map-marker').hover( function(e){
					setBocadillo( $(this).attr('rel'), true )
				}, 
				function(e){
					setBocadillo( $(this).attr('rel'), false )
				} );
				
			$('.map-marker-big').hover( function(e){
					setBocadillo( -1, true )
				}, 
				function(e){
					setBocadillo( -1, false )
				} );
				
			var $win = $( '#win-marker' );
				
			// ipad
			$('.map-marker').click( function(){
				setBocadillo( $(this).attr('rel'), true );
			} );
			$('.map-marker-big').click( function(){
				setBocadillo( -1, true );
			} );	
			$win.click( function(){ $win.hide(); });	
			
			$('.row .indication').click( function(){
				$('.marker').removeClass( 'selected' );
				$('.indication').removeClass( 'selected' );
				$('.map-marker:eq(' + $(this).attr('rel') + ')').addClass( 'selected' ); 
				$(this).addClass( 'selected' );
			})		
			
			function setBocadillo( j, b ){
				var i = parseInt( j );
				var $marker = i < 0 ? $('.map-marker-big') : $('.map-marker:eq(' + i + ')');
				var $map = $('#map');
				
				if ( b ){
					var html = '<ul><li>';
					if ( $marker.attr('data-img') ){
						if ( $marker.attr('data-img') != '' ){
							html += '<img class="map-window" src="'+ $marker.attr('data-img') +'"></li><li>';
						}
					}
					var chamada_hotel = '<?php echo preg_replace( "/\r|\n/", "", get_field( 'chamada_hotel', 'options' ) ); ?>';
					html += (i < 0 ? '<div class="indication" rel="-1"><div class="i-title">' + chamada_hotel + '</div><div class="i-text"><p><strong></strong></p></div></div>' : $('.row .indication:eq(' + i + ')').clone()[0].outerHTML );
					html += '</li></ul>';
					
					
					$win.empty().html( html );
					
					var left = $marker.position().left - 40;
					if ( left + $win.width() > $map.width() ){
						left = $map.width() - $win.width() - 40;
					}
					var top = $marker.position().top - $win.height() - 40;
					if ( top < 20 ){
						top = 20;
						if ( $marker.position().left > $map.width()/2 ){
							// go left the marker
							left = $marker.position().left - $win.width() - 40;
						}
						else{
							// go right the marker
							left = $marker.position().left + 40;
						}
					}
					$win.css({left: left
						,top: top
					});
					$win.show();
				}
				else{
					$win.hide().empty();
				}
			}
			resizeMap();
		});
	}
	
	function resizeMap(){
		if ( $('#map').length > 0 ){
			var w = $window.width();
			var p;
			if ( w <= 480 ){
				//console.log( 'mobile' )
			}
			else if ( w <= 1024 ){
				//console.log( 'ipad' )
				p = 748/996;
			}
			else{
				//console.log( 'desktop' )
				p = 1;
			}
			if ( p ){
				$('.marker').each(function(index, element) {
					var coor = $(this).attr('data-rel').split( ',' );
					$(this).css( {
						left: parseInt(coor[0])*p,
						top: parseInt(coor[1])*p		
					});
				   $(this).css( {
						left:$(this).position().left - $(this).width()/2,
						top:$(this).position().top - $(this).height()/2		
					}); 
				});
			}
		}
	}
	
	function initMaps(){
		
		if ( ! document.getElementById("google-map") ) return;
		
		
		var ip = document.title.indexOf('Ipanema') != -1;
		
		
		var ipanema = new google.maps.LatLng(-22.985482, -43.207283);
		var arpoador = new google.maps.LatLng(-22.988501, -43.191807);
		var latlang = ip ? ipanema : arpoador;
		var zoom = ip ? 14 : 15;
		
		
		var mapOptions = {
		  center: latlang,
		  zoom: zoom,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		  
		  ,mapTypeControl: false
		};
		var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);
			
		var styles = [
		  {
			stylers: [
			  { saturation: -70 }
			]
		  }
		];
		map.setOptions({styles: styles});
	
	
		
		var beachMarker = new google.maps.Marker({
		  position: latlang,
		  map: map,
		  icon: TEMPLATE_URL + '/imgs/mapicon.png'
	  });
	  
	}
    </script>

   >

<?php wp_footer(); ?>
<div class="bg-modal" style="
	display: none;">
	<div class="box">
		<img src="<?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail.png"
     srcset="<?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail@2x.png 2x, 
             <?php echo get_template_directory_uri() ?>/imgs/modal/hotel-arpoador-gmail@3x.png 3x"
     class="hotel-arpoador-gmail">
     <span class="line"></span>
     <div class="Caro-hspede-Sua-re">
     	<?php the_field( 'mensagem', 'options' ); ?>
     </div>
     <div class="Por-favor-aguarde">
     	<img src="<?php echo get_template_directory_uri() ?>/imgs/modal/load.gif"
     class="Fill-15"> <?php the_field( 'texto_aguarde', 'options' ); ?>
     </div>
	</div>
</div>

</body>
</html>