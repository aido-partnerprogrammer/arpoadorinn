<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>


<div id="main" class="cityandsea-page error404 no-results not-found">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
		<?php get_bar(); ?>
    </div>

	<div id="page-wrapper" class="margin-wide">

        <section>
            <h1 class="tcenter"><?php the_field( 'titulo_404', 'options' ); ?></h1>
            <div class="tcenter subtitle"><p><?php the_field( 'mensagem_404', 'options' ); ?></p></div>
            <div class="row clearfix">
                <div class="col3">
                </div>
                <div class="col3">
                    <hr/>
                </div>
                <div class="col3">
                </div>
            </div>
        </section>

    </div><!-- #main-wrapper -->


<?php get_footer(); ?>