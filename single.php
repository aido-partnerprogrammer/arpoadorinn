<?php
/**
 * Template Name: Single Page Template.
 *
 *
 * @package WordPress
 * @subpackage Hotel Bossa
 * @since Hotel Bossa 1.0
 */

get_header(); ?>


<div id="main" class="cityandsea-page">

    <!-- booking bar -->
    <div id="book-now-bar" class="page-wide">
    	<?php get_bar();?>
    </div>

	<div id="page-wrapper" class="margin-wide bgyellow">

    <?php 
        while ( have_posts() ) : the_post(); 
    	$category = get_the_category();
	?>

	<section class="">
        <?php  $header = get_field( 'pagina_de_cabecalho', 'options' ); ?>
        <h1 class="tcenter"><div id="logo-cityandsea"></div><?php echo $header->post_title; ?></h1>		
        <div class="tcenter subtitle"><?php echo apply_filters('the_content', $header->post_content); ?></div>
        
        <div id="article" class="clearfix">
            <div class="col-content">
            	<p class="cat"><a href="<?php echo get_category_link( $category[0]->cat_ID ); ?>"><?php echo $category[0]->cat_name;?></a></p>
            	<p class="tit"><?php the_title(); ?></p>
                <!--<p class="dst-italic sub-tit"><?php echo get_field('subtitle');?></p>-->
                <p class="dst-italic"><?php $date = get_field('date'); echo $date; ?></p>
                <img class="img-article" src="<?php $img = get_field('image'); echo $img['sizes']['large']; ?>" />
                <div class="clearfix">
                    <div class="text"><?php the_content(''); ?></div>
                    <div class="col-200">
                        <?php echo get_field('information');?>
                        <a><div class="btn-gris share">Share</div></a>
                        <a href="<?php echo get_field('link');?>" target="_blank"><div class="btn-gris g-maps"><?php the_field( 'view_google_maps', 'options' ); ?></div></a>
                    </div>
                </div>
                <hr/>
                <!-- Prev and Next post -->
                <ul class="nav-posts">
                <?php 
					$p = get_adjacent_post( false, '', true );
					if ( !empty($p) ):
					$href = get_permalink( $p->ID );
					?>
                    <li>
                    	<a href="<?php echo $href; ?>"><p class="more"><?php the_field( 'eventos_anteriores', 'option' );?></p></a>
                        <a href="<?php echo $href; ?>"><p class="tit"><?php echo $p->post_title; ?></p></a>
                        <p class="dst-italic"><?php $date = get_field('date', $p->ID); echo $date; ?></p>
                        <!--<a href="<?php echo $href; ?>" class="none"><p class="ecrp"><?php the_excerpt($p->ID); ?></p></a>-->
                        <p class="ecrp"><?php echo wp_trim_words( get_the_content($p->ID), $num_words = 20, '...<a href="'. get_permalink($p->ID) .'" class="more blocky"> ' . (get_field('read_more_text', 'options')) . '</a>' ); ?></p>
                    </li>
                    <?php
					endif;

					$p = get_adjacent_post( false, '', false );
					if ( !empty($p) ):
					$href = get_permalink( $p->ID );
					?>
                    <li>
                    	<a href="<?php echo $href; ?>">
                            <p class="more"><?php the_field( 'proximos_eventos', 'option' );?></p>
                        </a>
                        <a href="<?php echo $href; ?>"><p class="tit"><?php echo $p->post_title; ?></p></a>
                        <p class="dst-italic"><?php $date = get_field('date', $p->ID); echo $date; ?></p>
                        <p class="ecrp"><?php echo wp_trim_words( get_the_content($p->ID), $num_words = 20, '...<a href="'. get_permalink($p->ID) .'" class="more blocky"> ' . (get_field('read_more_text', 'options')) . '</a>' ); ?></p>
                    </li>
                    <?php
					endif;
				?>
                </ul>
            </div>
            
            <!-- Featured same category posts -->
            <div class="col-featured col4">
				<?php
                    $args = array( 
                        'post_type' => 'post', 
                        'posts_per_page' => 4, 
                        'category__in' => array( $category[0]->cat_ID ), 
                        'post__not_in' => array( $post->ID ) 
                    );
                    $query = new WP_Query( $args ); 
				    if ( $query->have_posts() ): 
				?>
                    <div class="tcenter more"><?php the_field( 'mais_sobre', 'options' );?><br/><span><a href="<?php echo get_category_link( $category[0]->cat_ID ); ?>"><?php echo $category[0]->cat_name;?></a><span></div>
					<?php
					while ( $query->have_posts() ): 
                    $query->the_post();
                    ?>	
                    <div class="">
                        <a href="<?php the_permalink(); ?>"><img class="thumb-article" src="<?php $img = get_field('image'); echo $img['sizes']['thumbnail-grid-4']; ?>" /></a>
                        <a href="<?php the_permalink(); ?>"><p class="tit"><?php the_title(); ?></p></a>
                        <p class="dst-italic"><?php $date = get_field('date'); echo $date; ?></p>                        
                		<p class="ecrp"><?php echo wp_trim_words( get_the_content(), $num_words = 30, '...<a href="'. get_permalink() .'" class="more blocky"> ' . (get_field('read_more_text', 'options')) . '</a>' ); ?></p>
                    </div>
                    <?php	
                	endwhile;
                	wp_reset_postdata();
				endif; 
                ?>
            </div>
        </div>
        
        
                
    </section>

<?php endwhile; ?>
    </div><!-- #main-wrapper -->

<?php get_footer(); ?>