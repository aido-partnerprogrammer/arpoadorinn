<?php 
	
function layout_langswitch($customID = false) {
    $navID = $customID ? $customID : 'lang-switch';
    $langs = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str'); ?>
	<nav id="<?php echo $navID; ?>">
	    <ul class="languages">
	    <?php     
	        foreach( $langs as $lang ){
	        $extraClass = $lang['code'] == ICL_LANGUAGE_CODE ? 'flag-active' : '';
	    ?>
	            <li>                                
	                <a href="<?php echo $lang['url']; ?>" title="<?php echo $lang['native_name']; ?>">
	                    <span class="flag <?php echo $extraClass ?>">
	                        <?php echo $lang['tag']; ?>
	                    </span>
	                </a>                                
	            </li>
	    <?php
	        } // end foreach $langs
	    ?>
	    </ul>
	</nav>
<?php 

}


function get_bar(){ ?>
<form action="#" class="f-booking" data-behavior="default_values datepicker booking" data-url="<?php echo get_field('form_book_url', 'options') ?>" >
    <input type="hidden" id="hotel" name="Hotel" value="">
    <input type="hidden" id="chain" name="Chain" value="">
    <ul>
        <li>
            <input type="text" class="input-date hasDatepicker" id="from" name="arrive" value="check-in" data-default="check-in">
        </li>
        <li>
            <input type="text" class="input-date hasDatepicker" id="to" name="depart" value="check-out" data-default="check-out">
        </li>
        
        <li>
            <div class="selector" id="uniform-adult"><span>2</span>
            <select id="adult" name="adult" data-behavior="uniform" style="opacity: 0;">
                <option value=""><?php the_field('select_qtd_adultos', 'options') ?></option>
                <option value="1">1</option>
                <option value="2" selected>2</option>
				<option value="3">3</option>
                <!--option value="4">4</option>
                <option value="5">5</option -->
            </select></div>
        </li>
        <li>
            <!-- the bt is deactivated so it send a basic mailto - to activate it remove the class "deactivated" + set the url as "#" -->
            <a href="#" id="book-now-bar-main" class="bt" data-behavior="popup"><?php the_field('texto_botao','options'); ?></a>
        </li>
    </ul>
</form>   
<div class="book-now-mobile btn"><?php the_field('texto_botao','options'); ?></div>
<?php
}
